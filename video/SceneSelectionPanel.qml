import QtQuick 2.0


Rectangle {
    id: spx
    property int itemHeight: 10
    property string sceneSource: ""
    property int contador: 1
    property int bandera: 0

    Timer {
        interval: 1000; running: true; repeat: true;
        onTriggered:
        {

        }

    }


    function plays()
    {
        sceneSource = "VideoFullScreen.qml"
        menu.state = "Multimedia"
    }

    Button {
        id: closeButton
        visible:false
        anchors {
            top: parent.top
            right: parent.right
            margins: spx.margins

        }
        width: Math.max(parent.width, parent.height) / 12
        height: Math.min(parent.width, parent.height) / 12
        z: 2.0
        bgColor: "#212121"
        bgColorSelected: "#757575"
        textColorSelected: "white"
        text: "Regresar"
        onClicked:
        {
            menu.state = "Multimedia"
        }
    }





    ListModel {
        id: videolist
        ListElement { name: "Reproducir"; source: "VideoFullScreen.qml" }
    }

    Component {
        id: leftDelegate
        Item {
            width: 200
            height: 50

            Button {
                anchors.fill: parent
                anchors.margins: 0
                anchors.rightMargin: 0
                anchors.bottomMargin: 0
                text: name
                visible: false
                onClicked:
                {
                    spx.sceneSource = source
                    spx.visible = false
                    menu.state = "Multimedia"
                }
            }

        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: (itemHeight) + 10
        clip: true

        Row {
            id: layout
            anchors {
                fill: parent
                topMargin: 20
                bottomMargin: 20
            }

            Column {
                Repeater {
                    model: videolist
                    delegate: leftDelegate
                }
            }

        }
    }
}
