// Librerias
#include <QtCore/QStandardPaths>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtQml/QQmlContext>
#include <QtQml/QQmlEngine>
#include <QtGui/QGuiApplication>
#include <QtQuick/QQuickItem>
#include <QtQuick/QQuickView>
#include <QQmlComponent>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QTime>
#include <QBasicTimer>
#include <QDebug>
#include <QEasingCurve>
#include <QGeoCoordinate>
#include <QtPositioning/private/qgeoprojection_p.h>
#include <QDateTime>
#include "vanet.h"
#include "appmodel.h"
#include "gps.h"
//#include "wifi.h"

int main(int argc, char *argv[])
{
    // disable the above 2 lines before enabling this
    //WifiConnectionHandler connectionHandler;
    //connectionHandler.registerUserData();f



    // Registros
    qmlRegisterType<WeatherData>("WeatherInfo", 1, 0, "WeatherData");
    qmlRegisterType<AppModel>("WeatherInfo", 1, 0, "AppModel");
    qRegisterMetaType<WeatherData>();

    QGuiApplication app(argc, argv);

    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QUrl url1, url2;
    QString source1, source2;
    qreal volume = 0.5;
    bool sourceIsUrl = false;

    if (sourceIsUrl) {
        url1 = source1;
        url2 = source2;
    } else {
        if (!source1.isEmpty())
            url1 = QUrl::fromLocalFile(source1);
        if (!source2.isEmpty())
            url2 = QUrl::fromLocalFile(source2);
    }

    GPS gps;
    gps.openSerialPort();

    VANET vanet;
    vanet.setLatitude("19.280757");
    vanet.setLongitude("-103.729053");
    vanet.setLatitudeAccidente("19.283248");
    vanet.setLongitudeAccidente("-103.731414");
    vanet.setLatitudeVehicle("19.280757");
    vanet.setLongitudeVehicle("-103.729053");
    vanet.setLatitudeTrafico("19.272241 ");
    vanet.setLongitudeTrafico("-103.715790");
    vanet.setLatitudeTrafico("19.272241 ");
    // 1
    vanet.setLongitudeEstacion1("-103.722188");
    vanet.setLatitudeEstacion1("19.285525");
    vanet.setColorO3("1");
    vanet.setColorSO2("2");
    vanet.setColorNO2("3");
    vanet.setColorCO("4");
    vanet.setColorPM10("5");
    vanet.setColorPM25("1");
    //2
    vanet.setLongitudeEstacion2("-103.723949");
    vanet.setLatitudeEstacion2("19.272475");
    vanet.setColorO3s2("4");
    vanet.setColorSO2s2("4");
    vanet.setColorNO2s2("3");
    vanet.setColorCOs2("2");
    vanet.setColorPM10s2("1");
    vanet.setColorPM25s2("4");
    // Temperatura del carrov
    vanet.setTemperature("35 °C");



    QQuickView viewer;
    viewer.rootContext()->setContextProperty("GPS", &gps);
    viewer.rootContext()->setContextProperty("VANET", &vanet);
    viewer.setSource(QUrl("qrc:/qml/qmlvideo/main.qml"));
    QObject::connect(viewer.engine(), SIGNAL(quit()), &viewer, SLOT(close()));
    QQuickItem *rootObject = viewer.rootObject();
    rootObject->setProperty("source1", url1);
    rootObject->setProperty("source2", url2);
    rootObject->setProperty("volume", volume);
    const QStringList moviesLocation = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation);
    const QUrl videoPath =
            QUrl::fromLocalFile(moviesLocation.isEmpty() ?
                                    app.applicationDirPath() :
                                    moviesLocation.front());
    viewer.rootContext()->setContextProperty("videoPath", videoPath);
    QMetaObject::invokeMethod(rootObject, "init");
    viewer.setMinimumSize(QSize(640, 360));
    viewer.show();

    return app.exec();
}

