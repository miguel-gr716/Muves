/****************************************************************************
** Meta object code from reading C++ file 'vanet.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "vanet.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'vanet.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_VANET_t {
    QByteArrayData data[142];
    char stringdata0[1942];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VANET_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VANET_t qt_meta_stringdata_VANET = {
    {
QT_MOC_LITERAL(0, 0, 5), // "VANET"
QT_MOC_LITERAL(1, 6, 8), // "finished"
QT_MOC_LITERAL(2, 15, 0), // ""
QT_MOC_LITERAL(3, 16, 14), // "QNetworkReply*"
QT_MOC_LITERAL(4, 31, 5), // "reply"
QT_MOC_LITERAL(5, 37, 13), // "insertVehicle"
QT_MOC_LITERAL(6, 51, 15), // "acknowledgement"
QT_MOC_LITERAL(7, 67, 8), // "latitude"
QT_MOC_LITERAL(8, 76, 11), // "temperature"
QT_MOC_LITERAL(9, 88, 9), // "vehicleId"
QT_MOC_LITERAL(10, 98, 14), // "packageCounter"
QT_MOC_LITERAL(11, 113, 5), // "speed"
QT_MOC_LITERAL(12, 119, 9), // "timestamp"
QT_MOC_LITERAL(13, 129, 9), // "longitude"
QT_MOC_LITERAL(14, 139, 9), // "vehicleIp"
QT_MOC_LITERAL(15, 149, 14), // "insertVehicle_"
QT_MOC_LITERAL(16, 164, 20), // "ultimaLecturaTrafico"
QT_MOC_LITERAL(17, 185, 15), // "jamSearchRadius"
QT_MOC_LITERAL(18, 201, 21), // "jamDetectionTimeFrame"
QT_MOC_LITERAL(19, 223, 24), // "ultimaLecturaIndicadores"
QT_MOC_LITERAL(20, 248, 15), // "envSearchRadius"
QT_MOC_LITERAL(21, 264, 28), // "insertIndicadoresAmbientales"
QT_MOC_LITERAL(22, 293, 9), // "stationId"
QT_MOC_LITERAL(23, 303, 9), // "stationIp"
QT_MOC_LITERAL(24, 313, 3), // "mO3"
QT_MOC_LITERAL(25, 317, 4), // "mNO2"
QT_MOC_LITERAL(26, 322, 4), // "mSO2"
QT_MOC_LITERAL(27, 327, 3), // "mCO"
QT_MOC_LITERAL(28, 331, 5), // "mPM10"
QT_MOC_LITERAL(29, 337, 5), // "mPM25"
QT_MOC_LITERAL(30, 343, 29), // "insertIndicadoresAmbientales_"
QT_MOC_LITERAL(31, 373, 11), // "maximoImeca"
QT_MOC_LITERAL(32, 385, 17), // "maximoImecaString"
QT_MOC_LITERAL(33, 403, 13), // "maximoImecas2"
QT_MOC_LITERAL(34, 417, 19), // "maximoImecaStrings2"
QT_MOC_LITERAL(35, 437, 7), // "getHour"
QT_MOC_LITERAL(36, 445, 7), // "setHour"
QT_MOC_LITERAL(37, 453, 5), // "value"
QT_MOC_LITERAL(38, 459, 11), // "getDateTime"
QT_MOC_LITERAL(39, 471, 11), // "setDateTime"
QT_MOC_LITERAL(40, 483, 12), // "getColorO3s2"
QT_MOC_LITERAL(41, 496, 12), // "setColorO3s2"
QT_MOC_LITERAL(42, 509, 13), // "getColorNO2s2"
QT_MOC_LITERAL(43, 523, 13), // "setColorNO2s2"
QT_MOC_LITERAL(44, 537, 13), // "getColorSO2s2"
QT_MOC_LITERAL(45, 551, 13), // "setColorSO2s2"
QT_MOC_LITERAL(46, 565, 12), // "getColorCOs2"
QT_MOC_LITERAL(47, 578, 12), // "setColorCOs2"
QT_MOC_LITERAL(48, 591, 14), // "getColorPM10s2"
QT_MOC_LITERAL(49, 606, 14), // "setColorPM10s2"
QT_MOC_LITERAL(50, 621, 14), // "getColorPM25s2"
QT_MOC_LITERAL(51, 636, 14), // "setColorPM25s2"
QT_MOC_LITERAL(52, 651, 20), // "getLatitudeEstacion1"
QT_MOC_LITERAL(53, 672, 20), // "setLatitudeEstacion1"
QT_MOC_LITERAL(54, 693, 21), // "getLongitudeEstacion1"
QT_MOC_LITERAL(55, 715, 21), // "setLongitudeEstacion1"
QT_MOC_LITERAL(56, 737, 20), // "getLatitudeEstacion2"
QT_MOC_LITERAL(57, 758, 20), // "setLatitudeEstacion2"
QT_MOC_LITERAL(58, 779, 21), // "getLongitudeEstacion2"
QT_MOC_LITERAL(59, 801, 21), // "setLongitudeEstacion2"
QT_MOC_LITERAL(60, 823, 18), // "getLatitudeTrafico"
QT_MOC_LITERAL(61, 842, 18), // "setLatitudeTrafico"
QT_MOC_LITERAL(62, 861, 19), // "getLongitudeTrafico"
QT_MOC_LITERAL(63, 881, 19), // "setLongitudeTrafico"
QT_MOC_LITERAL(64, 901, 18), // "getLatitudeVehicle"
QT_MOC_LITERAL(65, 920, 18), // "setLatitudeVehicle"
QT_MOC_LITERAL(66, 939, 19), // "getLongitudeVehicle"
QT_MOC_LITERAL(67, 959, 19), // "setLongitudeVehicle"
QT_MOC_LITERAL(68, 979, 20), // "getLatitudeAccidente"
QT_MOC_LITERAL(69, 1000, 20), // "setLatitudeAccidente"
QT_MOC_LITERAL(70, 1021, 21), // "getLongitudeAccidente"
QT_MOC_LITERAL(71, 1043, 21), // "setLongitudeAccidente"
QT_MOC_LITERAL(72, 1065, 12), // "getRespuesta"
QT_MOC_LITERAL(73, 1078, 18), // "setAcknowledgement"
QT_MOC_LITERAL(74, 1097, 11), // "getLatitude"
QT_MOC_LITERAL(75, 1109, 11), // "setLatitude"
QT_MOC_LITERAL(76, 1121, 12), // "getLongitude"
QT_MOC_LITERAL(77, 1134, 12), // "setLongitude"
QT_MOC_LITERAL(78, 1147, 14), // "getTemperature"
QT_MOC_LITERAL(79, 1162, 14), // "setTemperature"
QT_MOC_LITERAL(80, 1177, 17), // "getPackageCounter"
QT_MOC_LITERAL(81, 1195, 17), // "setPackageCounter"
QT_MOC_LITERAL(82, 1213, 8), // "getSpeed"
QT_MOC_LITERAL(83, 1222, 8), // "setSpeed"
QT_MOC_LITERAL(84, 1231, 12), // "getTimestamp"
QT_MOC_LITERAL(85, 1244, 12), // "setTimestamp"
QT_MOC_LITERAL(86, 1257, 18), // "getJamSearchRadius"
QT_MOC_LITERAL(87, 1276, 18), // "setJamSearchRadius"
QT_MOC_LITERAL(88, 1295, 24), // "getJamDetectionTimeFrame"
QT_MOC_LITERAL(89, 1320, 24), // "setJamDetectionTimeFrame"
QT_MOC_LITERAL(90, 1345, 13), // "getVehicle_id"
QT_MOC_LITERAL(91, 1359, 13), // "setVehicle_id"
QT_MOC_LITERAL(92, 1373, 13), // "getVehicle_ip"
QT_MOC_LITERAL(93, 1387, 13), // "setVehicle_ip"
QT_MOC_LITERAL(94, 1401, 13), // "getStation_id"
QT_MOC_LITERAL(95, 1415, 13), // "setStation_id"
QT_MOC_LITERAL(96, 1429, 13), // "getStation_ip"
QT_MOC_LITERAL(97, 1443, 13), // "setStation_ip"
QT_MOC_LITERAL(98, 1457, 18), // "getEnvSearchRadius"
QT_MOC_LITERAL(99, 1476, 18), // "setEnvSearchRadius"
QT_MOC_LITERAL(100, 1495, 13), // "getJamAlertId"
QT_MOC_LITERAL(101, 1509, 13), // "setJamAlertId"
QT_MOC_LITERAL(102, 1523, 12), // "getJamLength"
QT_MOC_LITERAL(103, 1536, 12), // "setJamLength"
QT_MOC_LITERAL(104, 1549, 14), // "getJamAvgSpeed"
QT_MOC_LITERAL(105, 1564, 14), // "setJamAvgSpeed"
QT_MOC_LITERAL(106, 1579, 10), // "getImecaO3"
QT_MOC_LITERAL(107, 1590, 10), // "setImecaO3"
QT_MOC_LITERAL(108, 1601, 10), // "getColorO3"
QT_MOC_LITERAL(109, 1612, 10), // "setColorO3"
QT_MOC_LITERAL(110, 1623, 11), // "getImecaNO2"
QT_MOC_LITERAL(111, 1635, 11), // "setImecaNO2"
QT_MOC_LITERAL(112, 1647, 11), // "getColorNO2"
QT_MOC_LITERAL(113, 1659, 11), // "setColorNO2"
QT_MOC_LITERAL(114, 1671, 11), // "getImecaSO2"
QT_MOC_LITERAL(115, 1683, 11), // "setImecaSO2"
QT_MOC_LITERAL(116, 1695, 11), // "getColorSO2"
QT_MOC_LITERAL(117, 1707, 11), // "setColorSO2"
QT_MOC_LITERAL(118, 1719, 10), // "getImecaCO"
QT_MOC_LITERAL(119, 1730, 10), // "setImecaCO"
QT_MOC_LITERAL(120, 1741, 10), // "getColorCO"
QT_MOC_LITERAL(121, 1752, 10), // "setColorCO"
QT_MOC_LITERAL(122, 1763, 12), // "getImecaPM10"
QT_MOC_LITERAL(123, 1776, 12), // "setImecaPM10"
QT_MOC_LITERAL(124, 1789, 12), // "getColorPM10"
QT_MOC_LITERAL(125, 1802, 12), // "setColorPM10"
QT_MOC_LITERAL(126, 1815, 12), // "getImecaPM25"
QT_MOC_LITERAL(127, 1828, 12), // "setImecaPM25"
QT_MOC_LITERAL(128, 1841, 12), // "getColorPM25"
QT_MOC_LITERAL(129, 1854, 12), // "setColorPM25"
QT_MOC_LITERAL(130, 1867, 5), // "getO3"
QT_MOC_LITERAL(131, 1873, 5), // "setO3"
QT_MOC_LITERAL(132, 1879, 2), // "o3"
QT_MOC_LITERAL(133, 1882, 6), // "getNO2"
QT_MOC_LITERAL(134, 1889, 6), // "setNO2"
QT_MOC_LITERAL(135, 1896, 3), // "nO2"
QT_MOC_LITERAL(136, 1900, 7), // "getPM10"
QT_MOC_LITERAL(137, 1908, 7), // "setPM10"
QT_MOC_LITERAL(138, 1916, 4), // "pM10"
QT_MOC_LITERAL(139, 1921, 7), // "getPM25"
QT_MOC_LITERAL(140, 1929, 7), // "setPM25"
QT_MOC_LITERAL(141, 1937, 4) // "pM25"

    },
    "VANET\0finished\0\0QNetworkReply*\0reply\0"
    "insertVehicle\0acknowledgement\0latitude\0"
    "temperature\0vehicleId\0packageCounter\0"
    "speed\0timestamp\0longitude\0vehicleIp\0"
    "insertVehicle_\0ultimaLecturaTrafico\0"
    "jamSearchRadius\0jamDetectionTimeFrame\0"
    "ultimaLecturaIndicadores\0envSearchRadius\0"
    "insertIndicadoresAmbientales\0stationId\0"
    "stationIp\0mO3\0mNO2\0mSO2\0mCO\0mPM10\0"
    "mPM25\0insertIndicadoresAmbientales_\0"
    "maximoImeca\0maximoImecaString\0"
    "maximoImecas2\0maximoImecaStrings2\0"
    "getHour\0setHour\0value\0getDateTime\0"
    "setDateTime\0getColorO3s2\0setColorO3s2\0"
    "getColorNO2s2\0setColorNO2s2\0getColorSO2s2\0"
    "setColorSO2s2\0getColorCOs2\0setColorCOs2\0"
    "getColorPM10s2\0setColorPM10s2\0"
    "getColorPM25s2\0setColorPM25s2\0"
    "getLatitudeEstacion1\0setLatitudeEstacion1\0"
    "getLongitudeEstacion1\0setLongitudeEstacion1\0"
    "getLatitudeEstacion2\0setLatitudeEstacion2\0"
    "getLongitudeEstacion2\0setLongitudeEstacion2\0"
    "getLatitudeTrafico\0setLatitudeTrafico\0"
    "getLongitudeTrafico\0setLongitudeTrafico\0"
    "getLatitudeVehicle\0setLatitudeVehicle\0"
    "getLongitudeVehicle\0setLongitudeVehicle\0"
    "getLatitudeAccidente\0setLatitudeAccidente\0"
    "getLongitudeAccidente\0setLongitudeAccidente\0"
    "getRespuesta\0setAcknowledgement\0"
    "getLatitude\0setLatitude\0getLongitude\0"
    "setLongitude\0getTemperature\0setTemperature\0"
    "getPackageCounter\0setPackageCounter\0"
    "getSpeed\0setSpeed\0getTimestamp\0"
    "setTimestamp\0getJamSearchRadius\0"
    "setJamSearchRadius\0getJamDetectionTimeFrame\0"
    "setJamDetectionTimeFrame\0getVehicle_id\0"
    "setVehicle_id\0getVehicle_ip\0setVehicle_ip\0"
    "getStation_id\0setStation_id\0getStation_ip\0"
    "setStation_ip\0getEnvSearchRadius\0"
    "setEnvSearchRadius\0getJamAlertId\0"
    "setJamAlertId\0getJamLength\0setJamLength\0"
    "getJamAvgSpeed\0setJamAvgSpeed\0getImecaO3\0"
    "setImecaO3\0getColorO3\0setColorO3\0"
    "getImecaNO2\0setImecaNO2\0getColorNO2\0"
    "setColorNO2\0getImecaSO2\0setImecaSO2\0"
    "getColorSO2\0setColorSO2\0getImecaCO\0"
    "setImecaCO\0getColorCO\0setColorCO\0"
    "getImecaPM10\0setImecaPM10\0getColorPM10\0"
    "setColorPM10\0getImecaPM25\0setImecaPM25\0"
    "getColorPM25\0setColorPM25\0getO3\0setO3\0"
    "o3\0getNO2\0setNO2\0nO2\0getPM10\0setPM10\0"
    "pM10\0getPM25\0setPM25\0pM25"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VANET[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
     113,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  579,    2, 0x0a /* Public */,
       5,    9,  582,    2, 0x0a /* Public */,
      15,    9,  601,    2, 0x0a /* Public */,
      16,    7,  620,    2, 0x0a /* Public */,
      19,    6,  635,    2, 0x0a /* Public */,
      21,   11,  648,    2, 0x0a /* Public */,
      30,   11,  671,    2, 0x0a /* Public */,
      31,    0,  694,    2, 0x0a /* Public */,
      32,    0,  695,    2, 0x0a /* Public */,
      33,    0,  696,    2, 0x0a /* Public */,
      34,    0,  697,    2, 0x0a /* Public */,
      35,    0,  698,    2, 0x0a /* Public */,
      36,    1,  699,    2, 0x0a /* Public */,
      38,    0,  702,    2, 0x0a /* Public */,
      39,    1,  703,    2, 0x0a /* Public */,
      40,    0,  706,    2, 0x0a /* Public */,
      41,    1,  707,    2, 0x0a /* Public */,
      42,    0,  710,    2, 0x0a /* Public */,
      43,    1,  711,    2, 0x0a /* Public */,
      44,    0,  714,    2, 0x0a /* Public */,
      45,    1,  715,    2, 0x0a /* Public */,
      46,    0,  718,    2, 0x0a /* Public */,
      47,    1,  719,    2, 0x0a /* Public */,
      48,    0,  722,    2, 0x0a /* Public */,
      49,    1,  723,    2, 0x0a /* Public */,
      50,    0,  726,    2, 0x0a /* Public */,
      51,    1,  727,    2, 0x0a /* Public */,
      52,    0,  730,    2, 0x0a /* Public */,
      53,    1,  731,    2, 0x0a /* Public */,
      54,    0,  734,    2, 0x0a /* Public */,
      55,    1,  735,    2, 0x0a /* Public */,
      56,    0,  738,    2, 0x0a /* Public */,
      57,    1,  739,    2, 0x0a /* Public */,
      58,    0,  742,    2, 0x0a /* Public */,
      59,    1,  743,    2, 0x0a /* Public */,
      60,    0,  746,    2, 0x0a /* Public */,
      61,    1,  747,    2, 0x0a /* Public */,
      62,    0,  750,    2, 0x0a /* Public */,
      63,    1,  751,    2, 0x0a /* Public */,
      64,    0,  754,    2, 0x0a /* Public */,
      65,    1,  755,    2, 0x0a /* Public */,
      66,    0,  758,    2, 0x0a /* Public */,
      67,    1,  759,    2, 0x0a /* Public */,
      68,    0,  762,    2, 0x0a /* Public */,
      69,    1,  763,    2, 0x0a /* Public */,
      70,    0,  766,    2, 0x0a /* Public */,
      71,    1,  767,    2, 0x0a /* Public */,
      72,    0,  770,    2, 0x0a /* Public */,
      73,    1,  771,    2, 0x0a /* Public */,
      74,    0,  774,    2, 0x0a /* Public */,
      75,    1,  775,    2, 0x0a /* Public */,
      76,    0,  778,    2, 0x0a /* Public */,
      77,    1,  779,    2, 0x0a /* Public */,
      78,    0,  782,    2, 0x0a /* Public */,
      79,    1,  783,    2, 0x0a /* Public */,
      80,    0,  786,    2, 0x0a /* Public */,
      81,    1,  787,    2, 0x0a /* Public */,
      82,    0,  790,    2, 0x0a /* Public */,
      83,    1,  791,    2, 0x0a /* Public */,
      84,    0,  794,    2, 0x0a /* Public */,
      85,    1,  795,    2, 0x0a /* Public */,
      86,    0,  798,    2, 0x0a /* Public */,
      87,    1,  799,    2, 0x0a /* Public */,
      88,    0,  802,    2, 0x0a /* Public */,
      89,    1,  803,    2, 0x0a /* Public */,
      90,    0,  806,    2, 0x0a /* Public */,
      91,    1,  807,    2, 0x0a /* Public */,
      92,    0,  810,    2, 0x0a /* Public */,
      93,    1,  811,    2, 0x0a /* Public */,
      94,    0,  814,    2, 0x0a /* Public */,
      95,    1,  815,    2, 0x0a /* Public */,
      96,    0,  818,    2, 0x0a /* Public */,
      97,    1,  819,    2, 0x0a /* Public */,
      98,    0,  822,    2, 0x0a /* Public */,
      99,    1,  823,    2, 0x0a /* Public */,
     100,    0,  826,    2, 0x0a /* Public */,
     101,    1,  827,    2, 0x0a /* Public */,
     102,    0,  830,    2, 0x0a /* Public */,
     103,    1,  831,    2, 0x0a /* Public */,
     104,    0,  834,    2, 0x0a /* Public */,
     105,    1,  835,    2, 0x0a /* Public */,
     106,    0,  838,    2, 0x0a /* Public */,
     107,    1,  839,    2, 0x0a /* Public */,
     108,    0,  842,    2, 0x0a /* Public */,
     109,    1,  843,    2, 0x0a /* Public */,
     110,    0,  846,    2, 0x0a /* Public */,
     111,    1,  847,    2, 0x0a /* Public */,
     112,    0,  850,    2, 0x0a /* Public */,
     113,    1,  851,    2, 0x0a /* Public */,
     114,    0,  854,    2, 0x0a /* Public */,
     115,    1,  855,    2, 0x0a /* Public */,
     116,    0,  858,    2, 0x0a /* Public */,
     117,    1,  859,    2, 0x0a /* Public */,
     118,    0,  862,    2, 0x0a /* Public */,
     119,    1,  863,    2, 0x0a /* Public */,
     120,    0,  866,    2, 0x0a /* Public */,
     121,    1,  867,    2, 0x0a /* Public */,
     122,    0,  870,    2, 0x0a /* Public */,
     123,    1,  871,    2, 0x0a /* Public */,
     124,    0,  874,    2, 0x0a /* Public */,
     125,    1,  875,    2, 0x0a /* Public */,
     126,    0,  878,    2, 0x0a /* Public */,
     127,    1,  879,    2, 0x0a /* Public */,
     128,    0,  882,    2, 0x0a /* Public */,
     129,    1,  883,    2, 0x0a /* Public */,
     130,    0,  886,    2, 0x0a /* Public */,
     131,    1,  887,    2, 0x0a /* Public */,
     133,    0,  890,    2, 0x0a /* Public */,
     134,    1,  891,    2, 0x0a /* Public */,
     136,    0,  894,    2, 0x0a /* Public */,
     137,    1,  895,    2, 0x0a /* Public */,
     139,    0,  898,    2, 0x0a /* Public */,
     140,    1,  899,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,    6,    7,    8,    9,   10,   11,   12,   13,   14,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,    6,    7,    8,    9,   10,   11,   12,   13,   14,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,    9,   12,   13,    7,   17,   18,   14,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,    9,   12,   13,    7,   20,   14,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,   22,   23,   12,   24,   25,   26,   27,   28,   29,   10,    6,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,   22,   23,   12,   24,   25,   26,   27,   28,   29,   10,    6,
    QMetaType::Int,
    QMetaType::QString,
    QMetaType::Int,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,  132,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,  135,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,  138,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,  141,

       0        // eod
};

void VANET::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        VANET *_t = static_cast<VANET *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->finished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 1: _t->insertVehicle((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6])),(*reinterpret_cast< QString(*)>(_a[7])),(*reinterpret_cast< QString(*)>(_a[8])),(*reinterpret_cast< QString(*)>(_a[9]))); break;
        case 2: _t->insertVehicle_((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6])),(*reinterpret_cast< QString(*)>(_a[7])),(*reinterpret_cast< QString(*)>(_a[8])),(*reinterpret_cast< QString(*)>(_a[9]))); break;
        case 3: _t->ultimaLecturaTrafico((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6])),(*reinterpret_cast< QString(*)>(_a[7]))); break;
        case 4: _t->ultimaLecturaIndicadores((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6]))); break;
        case 5: _t->insertIndicadoresAmbientales((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6])),(*reinterpret_cast< QString(*)>(_a[7])),(*reinterpret_cast< QString(*)>(_a[8])),(*reinterpret_cast< QString(*)>(_a[9])),(*reinterpret_cast< QString(*)>(_a[10])),(*reinterpret_cast< QString(*)>(_a[11]))); break;
        case 6: _t->insertIndicadoresAmbientales_((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6])),(*reinterpret_cast< QString(*)>(_a[7])),(*reinterpret_cast< QString(*)>(_a[8])),(*reinterpret_cast< QString(*)>(_a[9])),(*reinterpret_cast< QString(*)>(_a[10])),(*reinterpret_cast< QString(*)>(_a[11]))); break;
        case 7: { int _r = _t->maximoImeca();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 8: { QString _r = _t->maximoImecaString();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 9: { int _r = _t->maximoImecas2();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 10: { QString _r = _t->maximoImecaStrings2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 11: { QString _r = _t->getHour();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 12: _t->setHour((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: { QString _r = _t->getDateTime();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 14: _t->setDateTime((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: { QString _r = _t->getColorO3s2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 16: _t->setColorO3s2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 17: { QString _r = _t->getColorNO2s2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 18: _t->setColorNO2s2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 19: { QString _r = _t->getColorSO2s2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 20: _t->setColorSO2s2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 21: { QString _r = _t->getColorCOs2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 22: _t->setColorCOs2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 23: { QString _r = _t->getColorPM10s2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 24: _t->setColorPM10s2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 25: { QString _r = _t->getColorPM25s2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 26: _t->setColorPM25s2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 27: { QString _r = _t->getLatitudeEstacion1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 28: _t->setLatitudeEstacion1((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 29: { QString _r = _t->getLongitudeEstacion1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 30: _t->setLongitudeEstacion1((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 31: { QString _r = _t->getLatitudeEstacion2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 32: _t->setLatitudeEstacion2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 33: { QString _r = _t->getLongitudeEstacion2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 34: _t->setLongitudeEstacion2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 35: { QString _r = _t->getLatitudeTrafico();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 36: _t->setLatitudeTrafico((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 37: { QString _r = _t->getLongitudeTrafico();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 38: _t->setLongitudeTrafico((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 39: { QString _r = _t->getLatitudeVehicle();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 40: _t->setLatitudeVehicle((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 41: { QString _r = _t->getLongitudeVehicle();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 42: _t->setLongitudeVehicle((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 43: { QString _r = _t->getLatitudeAccidente();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 44: _t->setLatitudeAccidente((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 45: { QString _r = _t->getLongitudeAccidente();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 46: _t->setLongitudeAccidente((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 47: { QString _r = _t->getRespuesta();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 48: _t->setAcknowledgement((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 49: { QString _r = _t->getLatitude();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 50: _t->setLatitude((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 51: { QString _r = _t->getLongitude();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 52: _t->setLongitude((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 53: { QString _r = _t->getTemperature();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 54: _t->setTemperature((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 55: { QString _r = _t->getPackageCounter();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 56: _t->setPackageCounter((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 57: { QString _r = _t->getSpeed();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 58: _t->setSpeed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 59: { QString _r = _t->getTimestamp();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 60: _t->setTimestamp((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 61: { QString _r = _t->getJamSearchRadius();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 62: _t->setJamSearchRadius((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 63: { QString _r = _t->getJamDetectionTimeFrame();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 64: _t->setJamDetectionTimeFrame((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 65: { QString _r = _t->getVehicle_id();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 66: _t->setVehicle_id((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 67: { QString _r = _t->getVehicle_ip();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 68: _t->setVehicle_ip((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 69: { QString _r = _t->getStation_id();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 70: _t->setStation_id((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 71: { QString _r = _t->getStation_ip();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 72: _t->setStation_ip((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 73: { QString _r = _t->getEnvSearchRadius();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 74: _t->setEnvSearchRadius((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 75: { QString _r = _t->getJamAlertId();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 76: _t->setJamAlertId((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 77: { QString _r = _t->getJamLength();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 78: _t->setJamLength((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 79: { QString _r = _t->getJamAvgSpeed();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 80: _t->setJamAvgSpeed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 81: { QString _r = _t->getImecaO3();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 82: _t->setImecaO3((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 83: { QString _r = _t->getColorO3();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 84: _t->setColorO3((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 85: { QString _r = _t->getImecaNO2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 86: _t->setImecaNO2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 87: { QString _r = _t->getColorNO2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 88: _t->setColorNO2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 89: { QString _r = _t->getImecaSO2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 90: _t->setImecaSO2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 91: { QString _r = _t->getColorSO2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 92: _t->setColorSO2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 93: { QString _r = _t->getImecaCO();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 94: _t->setImecaCO((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 95: { QString _r = _t->getColorCO();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 96: _t->setColorCO((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 97: { QString _r = _t->getImecaPM10();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 98: _t->setImecaPM10((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 99: { QString _r = _t->getColorPM10();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 100: _t->setColorPM10((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 101: { QString _r = _t->getImecaPM25();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 102: _t->setImecaPM25((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 103: { QString _r = _t->getColorPM25();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 104: _t->setColorPM25((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 105: { QString _r = _t->getO3();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 106: _t->setO3((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 107: { QString _r = _t->getNO2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 108: _t->setNO2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 109: { QString _r = _t->getPM10();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 110: _t->setPM10((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 111: { QString _r = _t->getPM25();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 112: _t->setPM25((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        }
    }
}

const QMetaObject VANET::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_VANET.data,
      qt_meta_data_VANET,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *VANET::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VANET::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_VANET.stringdata0))
        return static_cast<void*>(const_cast< VANET*>(this));
    return QObject::qt_metacast(_clname);
}

int VANET::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 113)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 113;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 113)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 113;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
