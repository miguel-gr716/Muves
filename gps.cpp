#include "gps.h"

GPS::GPS(QObject *parent) : QObject(parent)
{
    serial = new QSerialPort(this);
    connect(serial,SIGNAL(error(QSerialPort::SerialPortError)),this,SLOT(handleError()));
    connect(serial,SIGNAL(readyRead()),this,SLOT(readData()));
    openSerialPort();
}

QString GPS::getPosicion() const
{
    return posicion;
}

void GPS::setPosicion(const QString &value)
{
    posicion = value;
}

QString GPS::getVelocidad() const
{
    return velocidad;
}

void GPS::setVelocidad(const QString &value)
{
    velocidad = value;
}


double GPS::getLongitud() const
{
    //return -103.729053;
    return longitud.toDouble();
}

void GPS::setLongitud(const QString &value)
{
    longitud = value;
}

double GPS::getLatitud() const
{
    //return 19.280757;
    return latitud.toDouble();
}

void GPS::setLatitud(const QString &value)
{
    latitud = value;
}

void GPS::openSerialPort()
{

    serial->setPortName("ttyUSB0");
    serial->setBaudRate(QSerialPort::Baud4800);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    if (serial->open(QIODevice::ReadWrite))
    {
        qDebug()<<"CONECTADO";
    }
    else
    {
        //qDebug()<<"ERROR";
    }
}


void GPS::closeSerialPort()
{
    if (serial->isOpen())
    {
        serial->close();
    }

    qDebug()<<"DESCONECTADO";
}


void GPS::writeData(const QByteArray &data)
{
    serial->write(data);
}


void GPS::readData()
{
    QByteArray data = serial->readAll();
    supercadena += QString(data);
    if(supercadena.length() > 100) supercadena = "";
    interpretarSuperCadena(supercadena);
    //qDebug()<<supercadena;
}


void GPS::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        closeSerialPort();
    }
}

QString GPS::interpretarSuperCadena(QString cadena)
{

    QStringList lista;
    QString latitud, longitud;


    if(cadena.length() > 40)
    {
        lista = cadena.split('$');
    }

    else
    {
        return "";
    }


    for (int var = 0; var < lista.count(); ++var)
    {

        if(lista[var].contains("GPGGA"))
        {
            QString item = lista[var];


            if(item.length() > 40 )
            {
                lista = item.split(',');
                latitud = procesarLatitud(lista[var+1]);
                longitud = procesarLongitud(lista[var+3]);
                qDebug()<<latitud+","+longitud;
                setPosicion(latitud+","+longitud);
            }
        }


        if(lista[var].contains("GPVTG") )
        {
            QString item = lista[var];
            if(item.length() > 40)
            {
                lista = item.split('N');
                item = lista[var];
                lista = item.split(',');
                velocidad = lista[var];
                qDebug()<<velocidad;
            }
        }


    }
    return QString::number(lista.length());
}


QString GPS::procesarLongitud(QString l)
{
    int longitudInt = l.toDouble()/100;
    double longitudDouble = (l.toDouble() -(longitudInt * 100))/60;
    if ((-longitudInt-longitudDouble)== 0) return l;
    longitud = QString::number(-longitudInt-longitudDouble,1,6);
    return longitud;
}


QString GPS::procesarLatitud(QString l)
{
    int latitudInt = l.toDouble()/100;
    double latitudDouble = (l.toDouble() -(latitudInt * 100))/60;
    if ((latitudInt+latitudDouble)== 0) return l;
    latitud = QString::number(latitudInt+latitudDouble,1,6);
    return latitud;
}








