// Librerias
import QtQuick 2.4
import QtLocation 5.5

MapQuickItem
{
    id: plane
    anchorPoint.x: image.width/2
    anchorPoint.y: image.height/2
    property string sourceIM

    sourceItem: Grid
    {
    columns: 1
    Grid
    {
        Image
        {
            id: carro
            width: 21
            height: 27
            source: sourceIM
        }
    }
}

}

