#ifndef GPS_H
#define GPS_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QDebug>

class GPS : public QObject
{
    Q_OBJECT
public:
    explicit GPS(QObject *parent = 0);
    QString supercadena;
    QSerialPort *serial;
    QString latitud;
    QString longitud;
    QString velocidad;
    QString posicion;

signals:

public slots:
    void openSerialPort();
    void closeSerialPort();
    void writeData(const QByteArray &data);
    void readData();
    void handleError(QSerialPort::SerialPortError error);
    QString interpretarSuperCadena(QString cadena);
    QString procesarLongitud(QString l);
    QString procesarLatitud(QString latitudL);
    double getLatitud() const;
    void setLatitud(const QString &value);
    double getLongitud() const;
    void setLongitud(const QString &value);
    QString getVelocidad() const;
    void setVelocidad(const QString &value);
    QString getPosicion() const;
    void setPosicion(const QString &value);

};

#endif // GPS_H
