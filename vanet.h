#ifndef VANET_H
#define VANET_H

#include <QObject>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include <QUrl>
#include <QStringList>
#include <qmath.h>

// MACROS VANET
#define urlUltimaLecturaTrafico           "http://192.168.151.13:8080/VANET/trafficJam/indicatorProcess"
#define urlVehicleInsert                  "http://192.168.151.13:8080/VANET/vehicleData/insert"
#define urlVehicleInsert_                 "http://192.168.151.13:8080/VANET/vehicleData/insert_"
#define urlIndicadoresAmbientalesInsert   "http://192.168.151.13:8080/VANET/environmentReport/insert"
#define urlIndicadoresAmbientalesInsert_  "http://192.168.151.13:8080/VANET/environmentReport/insert_"
#define urlUltimaLecIndAmbientales        "http://192.168.151.13:8080/VANET/environmentalIndicators/indicatorProcess"


#define pruebaInsertVehicle1              "{\"total\":1,\"responseMessage\":{\"nError\":\"\",\"msgError\":\"\"},\"success\":true,\"list\":[{\"vehicleIp\":\"100.123.121.100\",\"vehicleId\":\"2\"}]}"
#define pruebaInsertVehicle2              "{\"total\":2,\"responseMessage\":{\"nError\":\"\",\"msgError\":\"\"},\"success\":true,\"list\":[{\"vehicleIp\":\"100.123.121.100\",\"vehicleId\":\"2\"},{\"vehicleIp\":\"100.123.121.100\",\"vehicleId\":\"3\"}]}"
#define pruebaInsertIndAmb1               "{\"total\":1,\"responseMessage\":{\"nError\":\"\",\"msgError\":\"\"},\"success\":true,\"list\":[{\"stationIp\":\"100.123.121.100\",\"stationId\":\"c97f7820-309d-11e5-a151-feff819cdc9f \"}]}"
#define pruebaInsertIndAmb2               "{\"total\":2,\"responseMessage\":{\"nError\":\"\",\"msgError\":\"\"},\"success\":true,\"list\":[{\"stationIp\":\"100.123.121.100\",\"stationId\":\"c97f7820-309d-11e5-a151-feff819cdc9f \"},{\"stationIp\":\"100.123.121.100\",\"stationId\":\"c97f7820-309d-11e5-a151-feff819cdc9f\"}]}"
#define pruebaPeticionTrafico1            "{\"total\":1,\"responseMessage\":{\"nError\":\"\",\"msgError\":\"\"},\"success\":true,\"list\":[{\"vehicleIp\":\"100.123.121.100\",\"vehicleId\":\"2\",\"jamAlertId\":\"f8c5059a-8066-4112-a9de-902087687b52\",\"timestamp\":\"2015-10-22 14:00:10\",\"longitude\":\"-103.71252\",\"latitude\":\"19.26442\",\"jamLength\":\"98\",\"jamAvgSpeed\":\"9.9902\"}]}"
#define pruebaPeticionTrafico2            "{\"total\":1,\"responseMessage\":{\"nError\":\"\",\"msgError\":\"\"},\"success\":true,\"list\":[{\"vehicleIp\":\"100.123.121.100\",\"vehicleId\":\"2\",\"jamAlertId\":\"f8c5059a-8066-4112-a9de-902087687b52\",\"timestamp\":\"2015-10-22 14:00:10\",\"longitude\":\"-103.71252\",\"latitude\":\"19.26442\",\"jamLength\":\"98\",\"jamAvgSpeed\":\"9.9902\"},{\"vehicleIp\":\"100.123.121.100\",\"vehicleId\":\"5\",\"jamAlertId\":\"f8c5059a-8066-4112-a9de-902087687b52\",\"timestamp\":\"2015-10-22 14:00:10\",\"longitude\":\"-103.71252\",\"latitude\":\"19.26442\",\"jamLength\":\"98\",\"jamAvgSpeed\":\"9.9902\"}]}"
#define pruebaPeticionCalidadAire1        "{\"total\":1,\"responseMessage\":{\"nError\":\"\",\"msgError\":\"\"},\"success\":true,\"list\":[{\"vehicleIp\":\"100.123.121.100\",\"vehicleId\":\"2\",\"stationId\":\"2\",\"timestamp\":\"2\",\"imecaO3\":\"2\",\"colorO3\":\"2\",\"imecaNO2\":\"2\",\"colorNO2\":\"2\",\"imecaSO2\":\"2\",\"colorSO2\":\"2\",\"imecaCO\":\"2\",\"colorCO\":\"2\",\"imecaPM10\":\"2\",\"colorPM10\":\"2\",\"imecaPM25\":\"2\",\"colorPM25\":\"2\"}]}"
#define pruebaPeticionCalidadAire2        "{\"total\":1,\"responseMessage\":{\"nError\":\"\",\"msgError\":\"\"},\"success\":true,\"list\":[{\"vehicleIp\":\"100.123.121.100\",\"vehicleId\":\"2\",\"stationId\":\"2\",\"timestamp\":\"2\",\"imecaO3\":\"2\",\"colorO3\":\"2\",\"imecaNO2\":\"2\",\"colorNO2\":\"2\",\"imecaSO2\":\"2\",\"colorSO2\":\"2\",\"imecaCO\":\"2\",\"colorCO\":\"2\",\"imecaPM10\":\"2\",\"colorPM10\":\"2\",\"imecaPM25\":\"2\",\"colorPM25\":\"2\"},{\"vehicleIp\":\"100.123.121.100\",\"vehicleId\":\"2\",\"stationId\":\"2\",\"timestamp\":\"2\",\"imecaO3\":\"2\",\"colorO3\":\"2\",\"imecaNO2\":\"2\",\"colorNO2\":\"2\",\"imecaSO2\":\"2\",\"colorSO2\":\"2\",\"imecaCO\":\"2\",\"colorCO\":\"2\",\"imecaPM10\":\"2\",\"colorPM10\":\"2\",\"imecaPM25\":\"2\",\"colorPM25\":\"2\"}]}"


class VANET : public QObject
{
    Q_OBJECT
public:
    explicit VANET(QObject *parent = 0);
    // propiedades
    QString respuesta;
    QString vehicle_id;
    QString vehicle_ip;
    QString station_id;
    QString station_ip;
    QString timestamp;
    QString longitude;
    QString latitude;
    QString envSearchRadius;
    QString jamAlertId;
    QString jamLength;
    QString jamAvgSpeed;
    QString imecaO3;
    QString colorO3;
    QString colorO3s2;
    QString imecaNO2;
    QString colorNO2;
    QString colorNO2s2;
    QString imecaSO2;
    QString colorSO2;
    QString colorSO2s2;
    QString imecaCO;
    QString colorCO;
    QString colorCOs2;
    QString imecaPM10;
    QString colorPM10;
    QString colorPM10s2;
    QString imecaPM25;
    QString colorPM25;
    QString colorPM25s2;
    QString acknowledgement;
    QString temperature;
    QString packageCounter;
    QString speed;
    QString jamSearchRadius;
    QString jamDetectionTimeFrame;
    QString mO3;
    QString mNO2;
    QString mPM10;
    QString mPM25;
    QString latitudeAccidente;
    QString longitudeAccidente;
    QString latitudeVehicle;
    QString longitudeVehicle;
    QString latitudeTrafico;
    QString longitudeTrafico;
    QString latitudeEstacion1;
    QString longitudeEstacion1;
    QString latitudeEstacion2;
    QString longitudeEstacion2;
    QString Hour;
    QString DateTime;
    QNetworkAccessManager *nam;




signals:

public slots:
    void finished(QNetworkReply *reply);
    void insertVehicle(QString acknowledgement, QString latitude, QString temperature, QString vehicleId, QString packageCounter, QString speed, QString timestamp, QString longitude, QString vehicleIp);
    void insertVehicle_(QString acknowledgement, QString latitude, QString temperature, QString vehicleId, QString packageCounter, QString speed, QString timestamp, QString longitude, QString vehicleIp);
    void ultimaLecturaTrafico(QString vehicleId, QString timestamp, QString longitude, QString latitude, QString jamSearchRadius, QString jamDetectionTimeFrame, QString vehicleIp);
    void ultimaLecturaIndicadores(QString vehicleId, QString timestamp, QString longitude, QString latitude, QString envSearchRadius, QString vehicleIp);
    void insertIndicadoresAmbientales(QString stationId, QString stationIp, QString timestamp, QString mO3, QString mNO2, QString mSO2, QString mCO, QString mPM10, QString mPM25, QString packageCounter, QString acknowledgement);
    void insertIndicadoresAmbientales_(QString stationId, QString stationIp, QString timestamp, QString mO3, QString mNO2, QString mSO2, QString mCO, QString mPM10, QString mPM25, QString packageCounter, QString acknowledgement);
    int maximoImeca();
    QString maximoImecaString();
    int maximoImecas2();
    QString maximoImecaStrings2();
    // GET Y SET
    QString getHour() const;
    void setHour(const QString &value);
    QString getDateTime() const;
    void setDateTime(const QString &value);
    QString getColorO3s2() const;
    void setColorO3s2(const QString &value);
    QString getColorNO2s2() const;
    void setColorNO2s2(const QString &value);
    QString getColorSO2s2() const;
    void setColorSO2s2(const QString &value);
    QString getColorCOs2() const;
    void setColorCOs2(const QString &value);
    QString getColorPM10s2() const;
    void setColorPM10s2(const QString &value);
    QString getColorPM25s2() const;
    void setColorPM25s2(const QString &value);
    QString getLatitudeEstacion1() const;
    void setLatitudeEstacion1(const QString &value);
    QString getLongitudeEstacion1() const;
    void setLongitudeEstacion1(const QString &value);
    QString getLatitudeEstacion2() const;
    void setLatitudeEstacion2(const QString &value);
    QString getLongitudeEstacion2() const;
    void setLongitudeEstacion2(const QString &value);
    QString getLatitudeTrafico() const;
    void setLatitudeTrafico(const QString &value);
    QString getLongitudeTrafico() const;
    void setLongitudeTrafico(const QString &value);
    QString getLatitudeVehicle() const;
    void setLatitudeVehicle(const QString &value);
    QString getLongitudeVehicle() const;
    void setLongitudeVehicle(const QString &value);
    QString getLatitudeAccidente() const;
    void setLatitudeAccidente(const QString &value);
    QString getLongitudeAccidente() const;
    void setLongitudeAccidente(const QString &value);
    QString getRespuesta() const;
    void setAcknowledgement(const QString &value);
    QString getLatitude() const;
    void setLatitude(const QString &value);
    QString getLongitude() const;
    void setLongitude(const QString &value);
    QString getTemperature() const;
    void setTemperature(const QString &value);
    QString getPackageCounter() const;
    void setPackageCounter(const QString &value);
    QString getSpeed() const;
    void setSpeed(const QString &value);
    QString getTimestamp() const;
    void setTimestamp(const QString &value);
    QString getJamSearchRadius() const;
    void setJamSearchRadius(const QString &value);
    QString getJamDetectionTimeFrame() const;
    void setJamDetectionTimeFrame(const QString &value);
    QString getVehicle_id() const;
    void setVehicle_id(const QString &value);
    QString getVehicle_ip() const;
    void setVehicle_ip(const QString &value);
    QString getStation_id() const;
    void setStation_id(const QString &value);
    QString getStation_ip() const;
    void setStation_ip(const QString &value);
    QString getEnvSearchRadius() const;
    void setEnvSearchRadius(const QString &value);
    QString getJamAlertId() const;
    void setJamAlertId(const QString &value);
    QString getJamLength() const;
    void setJamLength(const QString &value);
    QString getJamAvgSpeed() const;
    void setJamAvgSpeed(const QString &value);
    QString getImecaO3() const;
    void setImecaO3(const QString &value);
    QString getColorO3() const;
    void setColorO3(const QString &value);
    QString getImecaNO2() const;
    void setImecaNO2(const QString &value);
    QString getColorNO2() const;
    void setColorNO2(const QString &value);
    QString getImecaSO2() const;
    void setImecaSO2(const QString &value);
    QString getColorSO2() const;
    void setColorSO2(const QString &value);
    QString getImecaCO() const;
    void setImecaCO(const QString &value);
    QString getColorCO() const;
    void setColorCO(const QString &value);
    QString getImecaPM10() const;
    void setImecaPM10(const QString &value);
    QString getColorPM10() const;
    void setColorPM10(const QString &value);
    QString getImecaPM25() const;
    void setImecaPM25(const QString &value);
    QString getColorPM25() const;
    void setColorPM25(const QString &value);
    QString getO3() const;
    void setO3(const QString &o3);
    QString getNO2() const;
    void setNO2(const QString &nO2);
    QString getPM10() const;
    void setPM10(const QString &pM10);
    QString getPM25() const;
    void setPM25(const QString &pM25);
};

#endif // VANET_H
