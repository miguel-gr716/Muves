/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Mobility Components.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0
//import QtQuick.Controls 1.4


Rectangle {
    id: scene
    color: "black"
    property alias buttonHeight: closeButton.height
    property string source1
    property string source2
    property int contentWidth: parent.width / 2
    property real volume: 0.25
    property int parametro
    property int margins: 5
    property QtObject content
    property int positionMultimedia
    property int bandera:0
    property int salir:0
    property int contador:0

    signal close
    signal videoFramePainted
    signal myButtonClickedPlay
    signal myButtonClickedPause
    signal myButtonClickedVolumenUp
    signal myButtonClickedVolumenDown
    signal myButtonClickedMute
    signal multimedia
    signal positionSeek
    signal reset

    property real minutos
    property int segundos
    property int totalSegundos
    property int restantes
    property int seek
    property int temporal
    property int temporal2

    property string diaSemana: dia()
    property string diaNumero: Qt.formatDateTime(new Date(), "dd")
    property string month: Qt.formatDateTime(new Date(), "MMMM")
    property string year: Qt.formatDateTime(new Date(), "20yy")


    Timer
    {
        interval: 1000; running: true; repeat: true
        onTriggered:
        {
            text5Hora.text =  Qt.formatDateTime(new Date(), "hh:mm")
            text: diaSemana + "  " + diaNumero + "  " + month + "  " + year
        }
    }


    function dia()
    {
        switch(Qt.formatDateTime(new Date(), "dddd"))
        {
        case "lunes": return "Monday";
        case "martes": return "Tuesday";
        case "miercoles": return "Wesday";
        case "jueves": return "Thursday";
        case "viernes": return "Friday";
        case "sabado": return "Saturday";
        case "domingo": return "Sunday";
        }
    }


    MouseArea
    {
        x:0;y:0;width:800; height: 480;
        onClicked:
        {
            mySlider.visible = true;
        }
    }

    Timer {
        id:timerx
        interval: 10000; running: false; repeat: true;
        onTriggered:
        {

            if(contador > 0)
            {
                myTextDuration.visible = false
                myText2.visible = false
                timerx.running = false
                mySlider.visible = false
            }

            contador++
        }

    }

    Button {
        id: closeButton
        anchors {
            top: parent.top
            right: parent.right
            margins: scene.margins
        }
        width: Math.max(parent.width, parent.height) / 12
        height: Math.min(parent.width, parent.height) / 12
        z: 2.0
        bgColor: "#212121"
        bgColorSelected: "#757575"
        textColorSelected: "white"
        text: "Stop"
        visible:false
    }

    MouseArea
    {
        x:740
        y:405

        Text
        {
            visible: false
            id: myTextDuration
            anchors.centerIn: parent
            color: "white"
            text: "Duration"
            font.bold: true
            font.pixelSize: 16
        }

    }

    MouseArea
    {
        x:740
        y:425

        Text
        {
            id: myText
            anchors.centerIn: parent
            color: "white"
            font.bold: true
            font.pixelSize: 16
            text: (Math.round((Math.round(content.duration * (Math.round(mySliderHandle.x* ((1/730)*100))/100)))))
            visible: false
            onTextChanged:
            {

                if (contador > 0)
                {
                    timerx.running = true
                    myTextDuration.visible = true
                    myText2.visible = true
                }
                else
                {
                    contador++
                }



                if (myText.text < content.duration-(content.duration*0.01))
                {

                    scene.positionSeek.connect(content.position(text));
                    scene.positionSeek()
                }
            }
        }

        Text
        {
            visible: false
            property real minutos
            property int segundos
            property int totalSegundos
            property int restantes
            property int seek
            id: myText2
            anchors.centerIn: parent
            color: "white"
            font.bold: true
            font.pixelSize: 16
            totalSegundos: content.duration / 1000
            segundos: Math.round(content.duration * (Math.round(mySliderHandle.x* ((1/730)*100))/100))
            minutos : Math.floor(segundos/60000)
            restantes: (Math.floor(segundos/60) - minutos*1000)*(0.06)
            text: ceros(minutos) + ":" + ceros(restantes)
        }

    }

    MouseArea
    {
        width:70; height:45; x:5; y:80
        Button
        {
            id: stop
            width: 70; height: 45
            bgColor: "#212121"
            bgColorSelected: "#757575"
            textColorSelected: "white"
            text: "Stop"

            onClicked:
            {
              scene.reset.connect(menu.closeScene);
              scene.reset()
            }
        }

    }

    MouseArea
    {
        width:70; height:45; x:5; y:140
        Button
        {
            id: play
            width: 70; height: 45
            bgColor: "#212121"
            bgColorSelected: "#757575"
            textColorSelected: "white"
            text: "Play"
            onClicked:
            {

                scene.myButtonClickedPlay.connect(content.start);
                scene.myButtonClickedPlay()
            }
        }

    }

    MouseArea
    {
        id: mouseAreaPause
        width:70; height:45; x:5; y:200

        Button
        {
            id: pause
            width: 70; height: 45
            bgColor: "#212121"
            bgColorSelected: "#757575"
            textColorSelected: "white"
            text: "Pause"
            onClicked:
            {
                scene.myButtonClickedPause.connect(content.pause);
                scene.myButtonClickedPause()
            }
        }

    }


    MouseArea
    {
        width:70; height:45; x:5; y:260
        Button
        {

            id: volumenMas
            width: 70; height: 45
            bgColor: "#212121"
            bgColorSelected: "#757575"
            textColorSelected: "white"
            text: "Vol+"
            onClicked:
            {

                scene.myButtonClickedVolumenUp.connect(content.volumenUp);
                scene.myButtonClickedVolumenUp()
            }

        }

    }

    MouseArea
    {
        width:70; height:45; x:5; y:320
        Button
        {
            id: volumenMenos
            width: 70; height: 45
            bgColor: "#212121"
            bgColorSelected: "#757575"
            textColorSelected: "white"
            text: "Vol-"
            onClicked:
            {
                scene.myButtonClickedVolumenDown.connect(content.volumenDown);
                scene.myButtonClickedVolumenDown()
            }
        }

    }

    MouseArea
    {
        width:70; height:45; x:5; y:380
        Button
        {
            id: mute
            width: 70; height: 45
            bgColor: "#212121"
            bgColorSelected: "#757575"
            textColorSelected: "white"
            text: "Mute"
            onClicked:
            {

                scene.myButtonClickedMute.connect(content.mute);
                scene.myButtonClickedMute()
            }
        }

    }

    Rectangle {

        visible:false
        id: mySlider

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            leftMargin: 20
            rightMargin: 20
            bottomMargin: 20
        }

        height: 20
        radius: 10
        smooth: true
        gradient: Gradient {
            GradientStop { position: 0.0; color: "white" }
            GradientStop { position: 1.0; color: "gray" }
        }

        Rectangle {
            id: mySliderHandle

            x: 1; y: 1; width: 30; height: 20
            radius: 10
            smooth: true
            color: "#757575"


            onXChanged:
            {
                //myText2.text = bandera = 2
            }

            MouseArea {
                anchors.fill: parent
                drag.target: parent
                drag.axis: Drag.XAxis
                drag.minimumX: 0
                drag.maximumX: parent.parent.width - 30
            }
        }
    }

    function ceros(parametro)
    {
        if(parametro < 10) return "0"+ parametro
        else return parametro
    }

    function pMultimedia(parametro)
    {
        positionMultimedia = parametro
    }

    // BorderImage
    BorderImage {
        id: borderImageHome4G
        x: 669; y: 18;
        width: 30
        height: 30
        opacity: 1
        source: "qrc:/Imagenes/images/4g-01.png"
        visible: true
    }

    BorderImage {
        id: borderImageHomeUsuario
        x: 753
        y: 18
        width: 20
        height: 20
        opacity: 1
        source: "qrc:/Imagenes/images/usuario-01.png"
        visible: true
    }

    BorderImage {
        id: borderImageHomeWifi
        x: 618; y: 18;
        width: 30
        height: 30
        opacity: 1
        source: "qrc:/Imagenes/images/wifi-01.png"
        visible: true
    }

    Text {
        id: text6Temperatura
        x: 221; y: 11; width: 119; height: 23;  font.bold: true; font.pixelSize: 20
        color: "#eefaf9"
        text: VANET.getTemperature()
        styleColor: "#00ffffff"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.AutoText
        visible: true
    }

    BorderImage {
        id: borderImageV2v
        x: 571; y: 18;
        width: 30
        height: 30
        opacity: 1
        source: "qrc:/Imagenes/images/v2v-01.png"
        visible: true
    }

    Text {
        id: text5Hora
        width: 196; height: 51;
        x: 51; y: 14;
        font.pixelSize: 50
        color: "#eefaf9"
        text: Qt.formatDateTime(new Date(), "hh:mm")
        styleColor: "#00ffffff"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.AutoText
        visible: true
    }

    Text {
        id: text7Fecha
        x: 240; y: 38; font.pixelSize: 20
        width: 241
        height: 15
        color: "#f9f9f9"
        text: diaSemana + "  " + diaNumero + "  " + month + "  " + year
        styleColor: "#00ffffff"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.AutoText
        visible: true
    }

    Text {
        id: text8Usuario
        x: 753; y: 39
        color: "#eefaf9"
        text: qsTr("User")
        styleColor: "#00ffffff"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.AutoText
        font.pixelSize: 12
        visible: true
    }


}

