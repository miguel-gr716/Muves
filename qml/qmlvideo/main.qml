////------------------------------ Librerias ---------------------------------------------------------------------
import QtQuick 2.0
import QtPositioning 5.6
import QtLocation 5.6
import QtMultimedia 5.0
import QtQuick.Dialogs 1.2
import WeatherInfo 1.0
import QtQuick.Controls 1.2
//import QtQuick.Enterprise.VirtualKeyboard 2.0

Rectangle {
    id:menu
    visible: true
    width: 800
    height: 480
    color: "#ff041e2b"
    // Propiedades
    property string source1
    property string source2
    property color bgColor: "black"
    property real volume: 0.25
    property int bandera: 0
    property int contador: 2
    property int contadorTrafico: 2
    property string textNodo: "Nodo 2"
    property string textColor: "Orange"
    property string intColor: "5"
    property string diaSemana: Qt.formatDateTime(new Date(), "dddd")
    property string diaNumero: Qt.formatDateTime(new Date(), "d")
    property string month: Qt.formatDateTime(new Date(), "MMMM")
    property string year: Qt.formatDateTime(new Date(), "20yy")

    
    FileBrowser 
    {
        id: fileBrowser1
        anchors.fill: menu
        Component.onCompleted: fileSelected.connect(menu.openFile1)
    }

    function openFile1(path)
    {
        menu.source1 = path
        sceneSelectionPanel.plays()
    }

    function init() {
        performanceLoader.init()
        fileBrowser1.folder = videoPath
    }

    function closeScene()
    {
        sceneSelectionPanel.sceneSource = ""
    }


    function configWIFI()
    {
        funciones.ocultarIconos();
        funciones.ocultarTextos();
        funciones.ocultaraAreas();
    }


    Item
    {
        id: video

        QtObject
        {
            id: d
            property int itemHeight: menu.height > menu.width ? menu.width / 10 : menu.height / 10
            property int buttonHeight: 0.8 * itemHeight
            property int margins: 5
        }

        Loader {
            id: performanceLoader

            Connections {
                target: inner
                onVisibleChanged:
                    if (performanceLoader.item)
                        performanceLoader.item.enabled = !inner.visible
                ignoreUnknownSignals: true
            }

            function init()
            {
                var enabled = menu.perfMonitorsLogging || menu.perfMonitorsVisible
                source = enabled ? "../performancemonitor/PerformanceItem.qml" : ""
            }


            onLoaded: {
                item.parent = menu
                item.anchors.fill = menu
                item.logging = menu.perfMonitorsLogging
                item.displayed = menu.perfMonitorsVisible
                item.enabled = false
                item.init()
            }
        }

        Rectangle {
            id: inner
            anchors.fill: parent
            color: menu.bgColor

            Rectangle {
                visible: false
                id: divider
                color: "black"
            }

            SceneSelectionPanel {
                visible:false
                id: sceneSelectionPanel
                itemHeight: d.itemHeight
                color: "#000000"
                anchors {
                    top: divider.bottom
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                radius:0
                onSceneSourceChanged: {
                    sceneLoader.source = sceneSource
                    var scene = null
                    var innerVisible = true
                    if (sceneSource == "") {
                        if (performanceLoader.item)
                            performanceLoader.item.videoActive = false
                    } else {
                        scene = sceneLoader.item
                        if (scene) {
                            if (scene.contentType === "video" && source1 === "") {
                                errorDialog.show("You must first select a video file")
                                sceneSource = ""
                            } else {
                                scene.parent = menu
                                scene.color = menu.bgColor
                                scene.buttonHeight = d.buttonHeight
                                scene.source1 = source1
                                scene.source2 = source2
                                scene.volume = volume
                                scene.anchors.fill = menu
                                scene.close.connect(closeScene)
                                scene.content.initialize()
                                innerVisible = false
                            }
                        }
                    }
                    videoFramePaintedConnection.target = scene
                    inner.visible = innerVisible
                }
            }
        }

        Loader {
            id: sceneLoader
        }

        Connections {
            id: videoFramePaintedConnection
            onVideoFramePainted: {
                if (performanceLoader.item)
                    performanceLoader.item.videoFramePainted()
            }
            ignoreUnknownSignals: true
        }

    }

    Item {
//        id: appContainer
//        anchors.left: parent.left
//        anchors.top: parent.top
//        anchors.right: parent.right
//        anchors.bottom: inputPanel.top
//        visible: true
        //--------------------------------- Weather ----------------------------------------------------------------------

//        AppModel
//        {
//            id: model
//            useGps:false
//            city: "Colima"
//            // Si ahi conexion y lee datos
//            onReadyChanged:
//            {
//                if (model.ready)
//                    menu.state = "ready"
//                else
//                    menu.state = "loading"
//            }
//        }

//        Item
//        {
//            id: wait
//            visible: false
//            anchors.fill: parent
//        }

//        Item {
//            id: main
//            visible: false
//            anchors.fill: parent

//            Text
//            {
//                x:10
//                y:110
//                font.pointSize: 25
//                color: "White"
//                text: (model.hasValidCity ? model.city : "Unknown location") + (model.useGps ? " (GPS)" : "")
//                horizontalAlignment: Text.AlignHCenter
//                verticalAlignment: Text.AlignVCenter
//            }


//            Column {

//                spacing: 6

//                anchors
//                {
//                    fill: parent
//                    topMargin: 40; bottomMargin: 6; leftMargin: 6; rightMargin: 6
//                }


//                BigForecastIcon
//                {
//                    id: current
//                    y:50
//                    width: main.width -12
//                    height: 2 * (main.height - 25 - 12) / 3

//                    weatherIcon: (model.hasValidWeather
//                                  ? model.weather.weatherIcon
//                                  : "01d")

//                    topText: (model.hasValidWeather
//                              ? model.weather.temperature
//                              : "")
//                    bottomText: (model.hasValidWeather
//                                 ? model.weather.weatherDescription
//                                 : "No weather data")
//                }

//                Row
//                {
//                    id: iconRow
//                    spacing: 6

//                    width: main.width - 12
//                    height: (main.height - 25 - 24) / 3

//                    property real iconWidth: iconRow.width / 4 - 10
//                    property real iconHeight: iconRow.height

//                    ForecastIcon {
//                        id: forecast1
//                        width: iconRow.iconWidth
//                        height: iconRow.iconHeight

//                        topText: (model.hasValidWeather ?
//                                      model.forecast[0].dayOfWeek : "")
//                        bottomText: (model.hasValidWeather ?
//                                         model.forecast[0].temperature : "")
//                        weatherIcon: (model.hasValidWeather ?
//                                          model.forecast[0].weatherIcon : "01d")
//                    }
//                    ForecastIcon {
//                        id: forecast2
//                        width: iconRow.iconWidth
//                        height: iconRow.iconHeight

//                        topText: (model.hasValidWeather ?
//                                      model.forecast[1].dayOfWeek : "")
//                        bottomText: (model.hasValidWeather ?
//                                         model.forecast[1].temperature : "")
//                        weatherIcon: (model.hasValidWeather ?
//                                          model.forecast[1].weatherIcon : "01d")
//                    }
//                    ForecastIcon {
//                        id: forecast3
//                        width: iconRow.iconWidth
//                        height: iconRow.iconHeight

//                        topText: (model.hasValidWeather ?
//                                      model.forecast[2].dayOfWeek : "")
//                        bottomText: (model.hasValidWeather ?
//                                         model.forecast[2].temperature : "")
//                        weatherIcon: (model.hasValidWeather ?
//                                          model.forecast[2].weatherIcon : "01d")
//                    }
//                    ForecastIcon {
//                        id: forecast4
//                        width: iconRow.iconWidth
//                        height: iconRow.iconHeight

//                        topText: (model.hasValidWeather ?
//                                      model.forecast[3].dayOfWeek : "")
//                        bottomText: (model.hasValidWeather ?
//                                         model.forecast[3].temperature : "")
//                        weatherIcon: (model.hasValidWeather ?
//                                          model.forecast[3].weatherIcon : "01d")
//                    }

//                }
//            }
        }


        Timer
        {
            interval: 1000; running: true; repeat: true
            onTriggered:
            {
                text5Hora.text =  Qt.formatDateTime(new Date(), "hh:mm")
                text: diaSemana + "  " + diaNumero + "  " + month + "  " + year
                //mapaOpenStreet.center = QtPositioning.coordinate(VANET.getLatitude(),VANET.getLongitude())
                //carro1.coordinate =  QtPositioning.coordinate(VANET.getLatitude(),VANET.getLongitude())
            }
        }


        Item
        {
            id:funciones

            function mes(dia)
            {
                switch(dia)
                {
                case "enero": return "january";
                case "febrero": return "febraury";
                case "marzo": return "march";
                case "abril": return "april";
                case "mayo": return "may";
                case "junio": return "june";
                case "julio": return "july";
                case "agosto": return "august";
                case "septiembre": return "septemmber";
                case "octubre": return "october";
                case "noviembre": return "november";
                case "diciembre": return "december";
                }
            }

            function diaSemana()
            {
                switch(Qt.formatDateTime(new Date(), "dddd"))
                {
                case "lunes": return "Monday";
                case "martes": return "Tuesday";
                case "miercoles": return "Wesday";
                case "jueves": return "Thursday";
                case "viernes": return "Friday";
                case "sabado": return "Saturday";
                case "domingo": return "Sunday";
                }
            }


            function multimedia()
            {
                menu.state = "Multimedia"
            }

            function estacion()
            {
                if( mapaMiniatura.textoT == " 1") return VANET.maximoImecaString();
                if( mapaMiniatura.textoT == " 2") return VANET.maximoImecaStrings2();
            }

            function estacionIndicador(indicador)
            {
                if( mapaMiniatura.textoT == " 1")
                {
                    switch(indicador)
                    {
                    case "O3": return VANET.getColorO3();
                    case "SO2": return VANET.getColorSO2();
                    case "NO2": return VANET.getColorNO2();
                    case "CO": return VANET.getColorCO();
                    case "PM10": return VANET.getColorPM10();
                    case "PM25": return VANET.getColorPM25();
                    }

                }
                if( mapaMiniatura.textoT == " 2")
                {
                    switch(indicador)
                    {
                    case "O3": return VANET.getColorO3s2();
                    case "SO2": return VANET.getColorSO2s2();
                    case "NO2": return VANET.getColorNO2s2();
                    case "CO": return VANET.getColorCOs2();
                    case "PM10": return VANET.getColorPM10s2();
                    case "PM25": return VANET.getColorPM25s2();
                    }

                }
            }

            function ocultaraAreas()
            {
                mouseAreaAdvertencia.visible= false
                mouseAreaBotonMultimedia.visible= false
                mouseAreaBotonAirQuality.visible= false
                mouseAreaBotonInternet.visible= false
                mouseAreaBotonWeather.visible= false
                mouseAreaBotonSettings.visible= false
                mouseAreaMapas.visible= false
                mouseAreaBotonLocalizacion.visible= false
                mouseAreaBotonTeclado.visible= false
                mouseAreaBotonTrafico.visible= false
                mouseAreaBuscar.visible= false
                mouseAreaLogo.visible= false
                mouseAreaSettings.visible= false
                mouseAreaBotonUsers.visible= false
                mouseAreaBotonDateHour.visible= false
                mouseAreaBotonConnectivity.visible= false
                mouseAreaBotonPreferences.visible= false
            }

            function ocultarTextos()
            {
                textTemperatura.visible= false
                textSitio.visible= false
                textDia1.visible= false
                textDia2.visible= false
                textDia3.visible= false
                textDia4.visible= false
                textDia5.visible= false
                textDia6.visible= false
                textClima.visible= false
                textData.visible= false
                textUser.visible= false
                textConectividad.visible= false
                textPreferences.visible= false
                text1Advertencia.visible= false
                text3Welcome.visible= false
                text4Copryht.visible= false
                text5Hora.visible= false
                text6Temperatura.visible= false
                text7Fecha.visible= false
                text8Usuario.visible= false
                text9Multimedia.visible = false
                text10Mapsx.visible= false
                text11AirQuality.visible= false
                text12Internet.visible= false
                text13Weather.visible= false
                text14Settings.visible= false
            }

            function ocultarIconos()
            {
                borderImageAdvertencia.visible = false;
            }

            function colorInttoScale(intColor)
            {
                switch(intColor)
                {
                case "1": return 50;
                case "2": return 100;
                case "3": return 150;
                case "4": return 200;
                case "5": return 250;
                }
            }

            function colorInttoName(intColor)
            {
                switch(intColor)
                {
                case "1": return "orange";
                case "2": return "yellow";
                case "3": return "Sienna";
                case "4": return "red";
                case "5": return "purple";
                }
            }


            function colorStationName(intColor)
            {
                if (intColor === "2")
                {
                    return "black"
                }
                else
                {   return "white"
                }
            }


            function colorInttoNamePromedio(intColor2)
            {
                switch(intColor)
                {
                case 1: return "orange";
                case 2: return "yellow";
                case 3: return "brown";
                case 4: return "red";
                case 5: return "purple";
                }
            }


            function mensajeImeca(intColor)
            {
                switch(intColor)
                {
                case "1": return qsTr("   Good  ");
                case "2": return qsTr("  Regular");
                case "3": return qsTr("    Bad  ");
                case "4": return qsTr("  Very\n  bad");
                case "5": return qsTr("  Extremely\n      bad");
                }
            }

            function recomendacionImeca(intColor)
            {
                switch(intColor)
                {
                case "1": return qsTr("Air quality is considered satisfactory, and air pollution has  little or no \nhealth risk.");
                case "2": return qsTr("Air quality is acceptable, though some contaminants may have  a moderate effect \non health for a small group of people sensitive to these substances.");
                case "3": return qsTr("There are some people who may have effects at lower concentrations.  The general \npopulation can not present a risk when the Imeca is in this range.");
                case "4": return qsTr("Members of sensitive groups may have serious discomfort. In this  interval prealert\nPhases are activated and Contingency Program .");
                case "5": return qsTr("When the index value is greater than 201 points (IMECA), the general population \nexperience serious health problems.");
                }
            }

        }


        Item
        {
            id: airQuality
            visible:false

            Item
            {
                id:item03

                Rectangle
                {
                    id:o3
                    x:50
                    y:100
                    width: 45
                    height: 35
                    color: "WhiteSmoke"
                    radius:4

                    Text
                    {
                        id:textO
                        color: "black"
                        text: qsTr(" O")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 25
                    }

                    Text
                    {
                        x:25
                        y:12
                        id:text3
                        color: "black"
                        text: qsTr("3")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 14
                    }
                }


                Rectangle
                {
                    id:colorO3
                    x:100
                    y:100
                    width: funciones.colorInttoScale(funciones.estacionIndicador("O3"))
                    height: 35
                    color: funciones.colorInttoName(funciones.estacionIndicador("O3"))
                    radius:4
                }
            }

            Item
            {
                id:itemS02

                Rectangle
                {
                    id:s02
                    x:50
                    y:150
                    width: 45
                    height: 35
                    color: "WhiteSmoke"
                    radius:4

                    Text
                    {
                        id:textS0
                        color: "black"
                        text: qsTr("SO")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 25
                    }

                    Text
                    {
                        x:30
                        y:17
                        id:text2
                        color: "black"
                        text: qsTr("2")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 14
                    }
                }


                Rectangle
                {
                    id:colorS02
                    x:100
                    y:150
                    width: funciones.colorInttoScale(funciones.estacionIndicador("SO2"))
                    height: 35
                    color: funciones.colorInttoName(funciones.estacionIndicador("SO2"))
                    radius:4
                }
            }

            Item
            {
                id:itemN02

                Rectangle
                {
                    id:n02
                    x:50
                    y:200
                    width: 45
                    height: 35
                    color: "WhiteSmoke"
                    radius:4

                    Text
                    {
                        id:textN0
                        color: "black"
                        text: qsTr("NO")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 25
                    }

                    Text
                    {
                        x:33
                        y:17
                        id:text2N
                        color: "black"
                        text: qsTr("2")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 14
                    }
                }


                Rectangle
                {
                    id:colorN02
                    x:100
                    y:200
                    width: funciones.colorInttoScale(funciones.estacionIndicador("NO2"))
                    height: 35
                    color: funciones.colorInttoName(funciones.estacionIndicador("NO2"))
                    radius:4
                }
            }

            Item
            {
                id:itemCO

                Rectangle
                {
                    id:c0
                    x:50
                    y:250
                    width: 45
                    height: 35
                    color: "WhiteSmoke"
                    radius:4

                    Text
                    {
                        id:textcO
                        color: "black"
                        text: qsTr(" C")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 25
                    }

                    Text
                    {
                        x:25
                        y:12
                        id:text0
                        color: "black"
                        text: qsTr("0")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 16
                    }
                }


                Rectangle
                {
                    id:colorCO
                    x:100
                    y:250
                    width: funciones.colorInttoScale(funciones.estacionIndicador("CO"))
                    height: 35
                    color: funciones.colorInttoName(funciones.estacionIndicador("CO"))
                    radius:4
                }
            }

            Item
            {
                id:itemPM10

                Rectangle
                {
                    id:pm10
                    x:50
                    y:300
                    width: 45
                    height: 35
                    color: "WhiteSmoke"
                    radius:4

                    Text
                    {
                        id:textPM
                        color: "black"
                        text: qsTr("PM")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 25
                    }

                    Text
                    {
                        x:32
                        y:17
                        id:text10
                        color: "black"
                        text: qsTr("10")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 12
                    }
                }


                Rectangle
                {
                    id:colorPM10
                    x:100
                    y:300
                    width: funciones.colorInttoScale(funciones.estacionIndicador("PM10"))
                    height: 35
                    color: funciones.colorInttoName(funciones.estacionIndicador("PM10"))
                    radius:4
                }
            }

            Item
            {
                id:itemPM25

                Rectangle
                {
                    id:pm25
                    x:50
                    y:350
                    width: 45
                    height: 35
                    color: "WhiteSmoke"
                    radius:4

                    Text
                    {
                        id:textPM2
                        color: "black"
                        text: qsTr("PM")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 25
                    }

                    Text
                    {
                        x:32
                        y:17
                        id:text25
                        color: "black"
                        text: qsTr("25")
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 12
                    }
                }


                Rectangle
                {
                    id:colorPM25
                    x:100
                    y:350
                    width: funciones.colorInttoScale(funciones.estacionIndicador("PM25"))
                    height: 35
                    color: funciones.colorInttoName(funciones.estacionIndicador("PM25"))
                    radius:4
                }
            }

            Item
            {
                id:resumen

                Rectangle
                {
                    id:resumenr
                    x:50
                    y:400
                    width: 100
                    height: 50
                    color: funciones.colorInttoName(funciones.estacion())

                    Text
                    {
                        id:resultado
                        color: funciones.colorStationName(funciones.estacion())
                        text: funciones.mensajeImeca(funciones.estacion())
                        styleColor: "#00ffffff"
                        font.pixelSize: 18
                    }
                }


                Rectangle
                {
                    id:resumenr2
                    x:150
                    y:400
                    width: 600
                    height: 50
                    color: funciones.colorInttoName(funciones.estacion())

                    Text
                    {

                        id:recomendacion
                        color: funciones.colorStationName(funciones.estacion())
                        text: funciones.recomendacionImeca(funciones.estacion())
                        styleColor: "#00ffffff"
                        textFormat: Text.AutoText
                        font.pixelSize: 16
                    }
                }

            }

            Item
            {
                id:nodo

                Rectangle
                {
                    id:nodor
                    x:500
                    y:100
                    width: 250
                    height: 40
                    color: funciones.colorInttoName(funciones.estacion())

                    Text
                    {
                        id:nodot
                        color: funciones.colorStationName(funciones.estacion())
                        text: textNodo
                        styleColor: "#00ffffff"
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        textFormat: Text.AutoText
                        font.pixelSize: 30
                    }
                }
            }
        }

        Item {
            id: iconos

            BorderImage {
                id: borderbackImeca
                x: 480
                y: 20
                width: 20
                height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/back imeca-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageLogo
                x: 280
                y: 87
                width: 100
                height: 100
                opacity: 0
            }

            BorderImage
            {
                id: borderGOOGLE
                x: 110
                y: 107
                width: 150
                height: 132
                opacity: 1
                source: "qrc:/Imagenes/images/multimedia-01.png"
                visible: false
            }

            BorderImage
            {
                id: borderMultimedia
                x: 110
                y: 107
                width: 150
                height: 132
                opacity: 1
                source: "qrc:/Imagenes/images/multimedia-01.png"
                visible: false
            }

            BorderImage {
                id: borderSettings
                x: 549; y: 280; width: 150; height: 130;
                opacity: 1
                source: "qrc:/Imagenes/images/ajustes-01.png"
                visible: false
            }

            BorderImage {
                id: borderWeather
                x: 334; y: 280; width: 150; height: 130;
                opacity: 1
                source: "qrc:/Imagenes/images/clima-01.png"
                visible: false
            }

            BorderImage {
                id: borderInternet
                x: 110; y: 280; width: 150; height: 130;
                opacity: 1
                source: "qrc:/Imagenes/images/internet-01.png"
                visible: false
            }

            BorderImage {
                id: borderAirQualyty
                x: 549; y: 107; width: 150; height: 130;
                opacity: 1
                source: "qrc:/Imagenes/images/calidad del aire-01.png"
                visible: false
            }

            BorderImage {
                id: borderMaps
                x: 333; y: 107; width: 150; height: 130;
                opacity: 1
                source: "qrc:/Imagenes/images/mapas-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageAdvertencia
                x: 331; y: 82;
                width: 178
                height: 134
                source: "qrc:/Imagenes/images/ALERTA-01.png"
                //visible:  false
            }

            BorderImage {
                id: imagen1x
                x:768
                y:12
                width: 15
                height: 15
                source: "qrc:/Imagenes/images/1x.jpg"
                visible: false
            }

            BorderImage {
                id: borderImageBackCalidadAire
                x: 750; y: 18;
                width: 30
                height: 30
                source: "qrc:/Imagenes/images/back imeca-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagex1
                x: 150; y: 100;
                width: 50
                height: 35
                source: "qrc:/Imagenes/images/verde.jpg"
                visible: false
            }

            BorderImage {
                id: borderImagex2
                x: 150; y: 140;
                width: 100
                height: 35
                source: "qrc:/Imagenes/images/amarillo.jpg"
                visible: false
            }

            BorderImage {
                id: borderImagex3
                x: 150; y: 180;
                width: 50
                height: 35
                source: "qrc:/Imagenes/images/verde.jpg"
                visible: false
            }

            BorderImage {
                id: borderImagex4
                x: 150; y: 220;
                width: 50
                height: 35
                source: "qrc:/Imagenes/images/verde.jpg"
                visible: false
            }

            BorderImage {
                id: borderImagex5
                x: 150; y: 260;
                width: 100
                height: 35
                source: "qrc:/Imagenes/images/amarillo.jpg"
                visible: false
            }

            BorderImage {
                id: borderImagex6
                x: 150; y: 300;
                width: 100
                height: 35
                source: "qrc:/Imagenes/images/amarillo.jpg"
                visible: false
            }

            BorderImage {
                id: borderImage03
                x: 100; y: 100;
                width: 40
                height: 35
                source: "qrc:/Imagenes/images/o3-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageS02
                x: 100; y: 140;
                width: 40
                height: 35
                source: "qrc:/Imagenes/images/s02-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageN02
                x: 100; y: 180;
                width: 40
                height: 35
                source: "qrc:/Imagenes/images/no2-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageC0
                x: 100; y: 220;
                width: 40
                height: 35
                source: "qrc:/Imagenes/images/co-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagePM10
                x: 100; y: 260;
                width: 40
                height: 35
                source: "qrc:/Imagenes/images/pm10-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageBackPM25
                x: 100; y: 300;
                width: 40
                height: 35
                source: "qrc:/Imagenes/images/pm2.5-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageM1
                x: 400; y: 120;
                width: 356
                height: 259
                source: "qrc:/Imagenes/images/m1.jpg"
                visible: false
            }

            BorderImage {
                id: borderImageM2
                x: 400; y: 120;
                width: 356
                height: 259
                source: "qrc:/Imagenes/images/m2.jpg"
                visible: false
            }

            BorderImage {
                id: borderImageK1
                x: 400; y: 70;
                width: 356
                height: 50
                source: "qrc:/Imagenes/images/knot1.jpg"
                visible: false
            }

            BorderImage {
                id: borderImageK2
                x: 400; y: 70;
                width: 356
                height: 50
                source: "qrc:/Imagenes/images/knot2.jpg"
                visible: false
            }

            BorderImage {
                id: borderImageRegular
                x:100 ; y: 400;
                width: 656
                height: 70
                source: "qrc:/Imagenes/images/regular.jpg"
                visible: false
            }

            BorderImage {
                id: borderImageLocalizacion
                x: 222
                y: 432
                width: 30
                height: 30
                source: "qrc:/Imagenes/images/ubicación-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageTecladoMapas
                x: 375
                y: 437
                width: 50
                height: 25
                source: "qrc:/Imagenes/images/teclado-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageTrafico
                x: 549
                y: 437
                width: 50
                height: 25
                source: "qrc:/Imagenes/images/trafico-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenDia1
                x: 150; y: 400; width: 40; height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/nublado-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenDiaActual
                x: 350; y: 180; width: 100; height: 100
                opacity: 1
                source: "qrc:/Imagenes/images/sol-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenDia2
                x: 250; y: 400; width: 30; height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/sol-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenDia3
                x: 350; y: 400;  width: 30; height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/sol-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenDia4
                x: 450; y: 400;  width: 40; height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/nublado-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenDia5
                x: 550; y: 400;  width: 40; height: 30
                opacity: 1
                source:  "qrc:/Imagenes/images/nublado-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenDia6
                x: 650; y: 400;  width: 30; height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/sol-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenUser
                x: 100; y: 150;  width: 130; height: 130
                opacity: 1
                source: "qrc:/Imagenes/images/usuarios-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenPreferencias
                x: 550; y: 150;  width: 130; height: 130
                opacity: 1
                source: "qrc:/Imagenes/images/preferencias-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenData
                x: 250; y: 150;  width: 130; height: 130
                opacity: 1
                source: "qrc:/Imagenes/images/hora-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenConectividad
                x: 400; y: 150;  width: 130; height: 130
                opacity: 1
                source: "qrc:/Imagenes/images/conectividad-01.png"
                visible: false
            }

            BorderImage {
                id: borderImagenBuscarClima
                x: 700; y: 100; width: 30; height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/buscar-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageGeolocalizacion
                x: 524; y: 17; width: 40; height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/geolocalización-01.png"
                visible: false

            }

            BorderImage {
                id: borderImageV2v
                x: 571; y: 18;
                width: 30
                height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/v2v-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageHomeWifi
                x: 618; y: 18;
                width: 30
                height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/wifi-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageHome4G
                x: 669; y: 18;
                width: 30
                height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/4g-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageRSSI4G
                x: 669; y: 18;
                width: 30
                height: 30
                opacity: 1
                visible: true
            }

            BorderImage {
                id: borderImageHomeUsuario
                x: 753
                y: 14
                width: 20
                height: 20
                opacity: 1
                source: "qrc:/Imagenes/images/usuario-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageHomeMultimedia
                x: 15; y: 17; width: 30; height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/home multimedia-01.png"
                visible: false

            }

            BorderImage {
                id: borderImageBackSettings
                x: 730; y: 17; width: 30; height: 30
                opacity: 1
                source: "qrc:/Imagenes/images/back ajustes-01.png"
                visible: false
            }

            BorderImage {
                id: borderImageBuscar
                x: 550
                y: 200
                width: 60
                height: 60
                source: "qrc:/Imagenes/images/buscar-01.png"
                visible: false
            }
        }

        Item
        {
            id: textos

            Text {
                id: text4Copryht
                x: 276
                y: 436
                width: 263; height: 27;
                color: "#eefaf9"
                text: qsTr("copyright ©")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                font.pixelSize: 10
                visible: false
            }

            Text {
                id: text3Welcome
                x: 289; y: 297;
                width: 263
                height: 121
                color: "#eefaf9"
                text: qsTr("Welcome User")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                font.pixelSize: 20
                visible: false
            }

            Text {
                id: textBuscar
                x: 230; y: 140;
                width: 305
                height: 174
                color: "#eefaf9"
                text: qsTr("Search")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                font.pixelSize: 70
                visible: false
            }

            Text {
                id: text14Settings
                opacity: 1
                text: qsTr("Settings")
                font.bold: true
                font.pixelSize: 16
                x: 596; y: 421;
                color: "#888888"
                styleColor: "#888888"
                visible: false
            }

            Text {
                id: text13Weather
                opacity: 1
                text: qsTr("Weather")
                font.bold: true
                font.pixelSize: 16
                x: 375; y: 421;
                color: "#7ebecd"
                styleColor: "#7ebecd"
                visible: false
            }

            Text {
                id: text12Internet
                opacity: 1
                text: qsTr("Internet")
                font.bold: true
                font.pixelSize: 16
                x: 153; y: 421;
                color: "#ffffff"
                styleColor: "#ffffff"
                visible: false
            }

            Text {
                id: text11AirQuality
                opacity: 1
                font.pixelSize: 16
                x: 582; y: 244;
                color: "#c0a92a"
                text: qsTr("Air Quality")
                font.bold: true
                styleColor: "#c0a92a"
                visible: false
            }

            Text {
                id: text10Mapsx
                opacity: 1
                x: 387; y: 244;
                color: "#e75353"; styleColor: "#e75353";
                text: qsTr("Maps")
                font.bold: true
                font.pixelSize: 16
                visible: false
            }

            Text {
                id: text9Multimedia
                opacity: 1
                x: 141; y: 245;
                color: "#00ea7b"
                text: qsTr("Multimedia")
                font.bold: true
                font.pixelSize: 16
                styleColor: "#00ea7b"
                visible: false
            }

            Text {
                id: text1Advertencia
                x: 261; y: 238;
                width: 305
                height: 174
                color: "#eefaf9"
                text: qsTr("Respect traffic signs                 Drive carefully                                 Wear your seatbelt                    Avoid distraccions while driving")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                font.pixelSize: 26
            }

            Text {
                id: textTemperatura
                width: 200; height: 50;
                x: 300; y: 300;
                font.pixelSize: 50
                color: "#eefaf9"
                text: qsTr("32 °C")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textSitio
                width: 400; height: 100;
                x: 200; y: 70;
                font.pixelSize: 18
                color: "#eefaf9"
                text: qsTr("Thursday 5 November     Colima,México.")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textDia1
                width: 50; height: 50;
                x: 145; y: 420;
                font.pixelSize: 12
                color: "#7EBECD"
                text: qsTr("Fry 36")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textDia2
                width: 50; height: 50;
                x: 240; y: 420;
                font.pixelSize: 12
                color: "#7EBECD"
                text: qsTr("Sat 27")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textDia3
                width: 50; height: 50;
                x: 340; y: 420;
                font.pixelSize: 12
                color: "#7EBECD"
                text: qsTr("Sun 28")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textDia4
                width: 50; height: 50;
                x: 445; y: 420;
                font.pixelSize: 12
                color: "#7EBECD"
                text: qsTr("Mon 29")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textDia5
                width: 50; height: 50;
                x: 545; y: 420;
                font.pixelSize: 12
                color: "#7EBECD"
                text: qsTr("Tue 30")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textDia6
                width: 50; height: 50;
                x: 640; y: 420;
                font.pixelSize: 12
                color: "#7EBECD"
                text: qsTr("Wed 31")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: text5Hora
                width: 196; height: 51;
                x: 51; y: 14;
                font.pixelSize: 50
                color: "#eefaf9"
                text: Qt.formatDateTime(new Date(), "hh:mm")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: text6Temperatura
                x: 240; y: 9; width: 119; height: 23;  font.bold: true; font.pixelSize: 20
                color: "#eefaf9"
                text: VANET.getTemperature()
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }


            Text
            {
                id: text7Fecha
                x: 240; y: 38; font.pixelSize: 14
                width: 241
                height: 15
                color: "#f9f9f9"
                text: diaSemana + "  " + diaNumero + "  " + month + "  " + year
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }


            Text
            {
                id: textClima
                x: 221; y: 9; width: 119; height: 23;  font.bold: true; font.pixelSize: 20
                color: "#eefaf9"
                text: qsTr("22.5°C")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: text8Usuario
                x: 753; y: 39
                color: "#eefaf9"
                text: qsTr("User")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                font.pixelSize: 12
                visible: false
            }

            Text {
                id: textData
                width: 100; height: 50;
                x: 260; y: 270;
                font.pixelSize: 15
                color: "#eefaf9"
                text: qsTr("Date/Hour")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textUser
                width: 50; height: 50;
                x: 130; y: 270;
                font.pixelSize: 15
                color: "#eefaf9"
                text: qsTr("Users")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textConectividad
                width: 100; height: 50;
                x: 420; y: 270;
                font.pixelSize: 15
                color: "#eefaf9"
                text: qsTr("Connectivity")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

            Text {
                id: textPreferences
                width: 100; height: 50;
                x: 570; y: 270;
                font.pixelSize: 15
                color: "#eefaf9"
                text: qsTr("Preferences")
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: false
            }

        }

        Map {

            id: mapaOpenStreet
            x:0
            y:70
            width: 800
            height: 350
            visible: false
            zoomLevel: 17

            center: QtPositioning.coordinate(VANET.getLatitude(),VANET.getLongitude())

            activeMapType : supportedMapTypes[2]


                    plugin: Plugin
                    {
                      name: "osm"
                    }



        TextField {
            id: passwordInput
            anchors.top: flagsLabel.bottom
            anchors.topMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * 0.36
            height: connectionButton.height * 1.1
            placeholderText: "Enter Direccion"
            visible: true
            font.pixelSize: 16
            inputMethodHints: Qt.ImhNoPredictiveText
        }

        PLANE
        {
            id: carro1
            coordinate:  QtPositioning.coordinate(VANET.getLatitude(),VANET.getLongitude())
            sourceIM: "qrc:/qml/qmlvideo/ubicación-01.png"
        }

        PLANE
        {
            id: accidente
            coordinate: QtPositioning.coordinate(VANET.getLatitudeAccidente(),VANET.getLongitudeAccidente())
            sourceIM: "qrc:/Imagenes/images/traffic.png"
        }


        PLANE
        {
            id: estacion1
            //19.285525, -103.722188
            coordinate: QtPositioning.coordinate(VANET.getLatitudeEstacion1(),VANET.getLongitudeEstacion1())
            bandera: true
            colorR: funciones.colorInttoName(VANET.maximoImecaString())
            textoC: funciones.colorStationName(VANET.maximoImecaString())
            texto: " 1"

            MouseArea
            {
                width: 50
                height: 50
                onClicked:
                {
                    funciones.ocultarIconos()
                    funciones.ocultarTextos()
                    funciones.ocultaraAreas()
                    fileBrowser1.visible = false
                    textNodo = "\t Node 1"
                    textColor = "orange"
                    menu.state = "Estacion1"
                }

            }
        }


        PLANE
        {
            id: estacion2
            // 19.272475, -103.723949
            coordinate: QtPositioning.coordinate(VANET.getLatitudeEstacion2(),VANET.getLongitudeEstacion2())
            bandera: true
            colorR: funciones.colorInttoName(VANET.maximoImecaStrings2())
            textoC: funciones.colorStationName(VANET.maximoImecaStrings2())
            texto: " 2"


            MouseArea
            {
                width: 50
                height: 50
                onClicked:
                {
                    funciones.ocultarIconos()
                    funciones.ocultarTextos()
                    funciones.ocultaraAreas()
                    fileBrowser1.visible = false
                    textNodo = "\t Node 2"
                    menu.state = "Estacion2"
                }

            }
        }


        MapCircle {
            id:circle
            visible:true
            center {
                latitude: VANET.getLatitudeTrafico()
                longitude: VANET.getLongitudeTrafico()
            }

            color: "#ff0000"
            border.color: "#190a33"
            border.width: 2
            smooth: true
            opacity: 0.25
            radius: 100.0
        }

        MapCircle {
            // Prueba
            id:circle2
            visible:true
            center {
                latitude: 19.269163
                longitude: -103.708022
            }

            color: "#ffff00"
            border.color: "#190a33"
            border.width: 2
            smooth: true
            opacity: 0.25
            radius: 100.0
        }
    }

    Map {

        id: mapaMiniatura
        x:500
        y:140
        width: 250
        height: 250
        visible: false
        zoomLevel: 17
        property string textoN: " 1"
        property string textoT: " 1"
        property string colorRR: "orange"

        activeMapType : supportedMapTypes[2]

        center // 3er anillo  19.280757, -103.729053v
        {
            latitude:  VANET.getLatitudeEstacion1()
            longitude: VANET.getLongitudeEstacion1()
        }

        plugin: Plugin
        {
        name: "osm"
    }

    PLANE
    {
        id: estacion1m
        coordinate: QtPositioning.coordinate( VANET.getLatitudeEstacion1(),VANET.getLongitudeEstacion1())
        bandera: true
        colorR: funciones.colorInttoName(funciones.estacion())
        texto: mapaMiniatura.textoT
        textoC: funciones.colorStationName(funciones.estacion())
    }


}

Item
{
    id:areasClick

    Item {
        id: generales

        MouseArea
        {
            id: mouseAreaBotonHome
            x: 15
            y: 17
            width: 30
            height: 30
            opacity: 0
            onClicked:
            {
                funciones.ocultarIconos()
                funciones.ocultarTextos()
                funciones.ocultaraAreas()
                menu.state = "3"
            }
        }

    }

    Item
    {
        id:mapas

        MouseArea
        {
            id: mouseAreaMapas
            x: 0
            y: 0
            width: 800
            height: 480
            visible:false
        }

        MouseArea
        {
            id: mouseAreaBotonLocalizacion
            x: 222
            y: 432
            width: 30
            height: 30
            onClicked:
            {
                if(contador++%2 == 0)
                {
                    carro1.visible = false;
                }
                else
                {
                    carro1.visible = true;
                }
            }
        }

        MouseArea {
            id: mouseAreaBotonTeclado
            x: 375
            y: 437
            width: 30
            height: 30
            //onClicked: menu.state = "Teclado"
        }

        MouseArea {
            id: mouseAreaBotonTrafico
            x: 549
            y: 437
            width: 50
            height: 25
            onClicked: {
                if(contadorTrafico++%2 == 0)
                {
                    circle.visible = false;
                    circle2.visible = false;
                    accidente.visible = false;
                }
                else
                {
                    circle.visible = true;
                    circle2.visible = true;
                    accidente.visible = true;
                }
            }
        }

    }


    Item {
        id: multimedia

        MouseArea {
            id: mouseAreaBuscarArchivos
            x:768
            y:12
            width: 15
            height: 15
        }

        MouseArea {
            id: mouseAreaBuscar
            x: 550
            y: 200
            width: 60
            height: 60
            onClicked:
            {
                menu.state = "Examinar"
                fileBrowser1.show()
            }
        }
    }
}

// Revisar Flujo para ponerlo en el item y refactorizarlo

MouseArea
{
    id: mouseAreaAdvertencia
    anchors.rightMargin: 0
    anchors.bottomMargin: 0
    anchors.leftMargin: 0
    anchors.topMargin: 0
    clip: false
    enabled: true
    hoverEnabled: true
    anchors.fill: parent
    onClicked:
    {
        funciones.ocultarIconos()
        funciones.ocultarTextos()
        funciones.ocultaraAreas()
        menu.state = "2"
    }
    visible:true
}

MouseArea {
    id: mouseAreaEleccionAPP
    anchors.rightMargin: 0
    anchors.bottomMargin: 0
    anchors.leftMargin: 0
    anchors.topMargin: 0
    clip: false
    enabled: true
    hoverEnabled: true
    anchors.fill: parent
    visible:false

    MouseArea {
        id: mouseAreaBotonMultimedia
        x: 98; y: 94; width: 148; height: 129;
        hoverEnabled: true
        opacity: 0
        visible:false
        onClicked:
        {
            funciones.ocultarIconos()
            funciones.ocultarTextos()
            funciones.ocultaraAreas()
            menu.state = "Multimedia"
        }
    }

    MouseArea {
        id: mouseAreaBotonMapas
        x: 276; y: 93; width: 150; height: 130;
        opacity: 0
        visible:false
        onClicked:
        {
            funciones.ocultarIconos()
            funciones.ocultarTextos()
            funciones.ocultaraAreas()
            fileBrowser1.visible = false
            menu.state = "Mapas"
        }
    }

    MouseArea {
        id: mouseAreaBotonAirQuality
        x: 449; y: 93; width: 150; height: 130;
        opacity: 0
        visible:false
        onClicked:
        {
            funciones.ocultarIconos()
            funciones.ocultarTextos()
            funciones.ocultaraAreas()
            menu.state = "Air Quality"
        }
    }

    MouseArea {
        id: mouseAreaBotonInternet
        x: 97; y: 259; width: 150; height: 130;
        opacity: 0
        visible:false
        onClicked:
        {
            funciones.ocultarIconos()
            funciones.ocultarTextos()
            funciones.ocultaraAreas()
            menu.state = "Internet"
        }
    }

    MouseArea {
        id: mouseAreaBotonWeather
        x: 356
        y: 256
        width: 100
        height: 100
        opacity: 0
        visible:false
        onClicked:
        {
            funciones.ocultarIconos()
            funciones.ocultarTextos()
            funciones.ocultaraAreas()
            menu.state = "Weather"
            model.refreshWeather()
        }
    }

    MouseArea {
        id: mouseAreaBotonSettings
        x: 477
        y: 254
        width: 100
        height: 100
        opacity: 0
        visible:false

        onClicked:
        {
            funciones.ocultarIconos()
            funciones.ocultarTextos()
            funciones.ocultaraAreas()
            borderMultimedia.visible = false;
            borderMaps.visible = false;
            borderAirQualyty.visible = false;
            borderInternet.visible = false;
            borderWeather.visible = false;
            borderSettings.visible = false;
            borderImageHome4G.visible = false;
            borderImageHomeUsuario.visible = false;
            borderImageHomeWifi.visible = false;
            borderImageV2v.visible = false;
            wifi.visible=true;

        }
    }

}

MouseArea {
    id: mouseAreaLogo
    anchors.rightMargin: 0
    anchors.bottomMargin: 0
    anchors.leftMargin: 0
    anchors.topMargin: 0
    clip: false
    enabled: true
    hoverEnabled: true
    anchors.fill: parent
    onClicked: menu.state = "3"
    visible:false
}

MouseArea {
    id: mouseAreaSettings
    x:0
    y:0
    width: 800
    height: 480
    opacity: 0
    visible: false

    MouseArea {
        id: mouseAreaBotonUsers
        x: 100; y: 150; width: 150; height: 150
        hoverEnabled: true
        opacity: 0
        visible:false
        //onClicked: menu.state = "Users"
    }

    MouseArea {
        id: mouseAreaBotonDateHour
        x: 250; y: 150; width: 150; height: 150
        hoverEnabled: true
        opacity: 0
        visible:false
        //onClicked: menu.state = "Dates"
    }

    MouseArea {
        id: mouseAreaBotonConnectivity
        x: 400; y: 150; width: 150; height: 150
        hoverEnabled: true
        opacity: 0
        visible:false
        onClicked:
        {
            menu.state = "3"
        }
    }

    MouseArea {
        id: mouseAreaBotonPreferences
        x: 550; y: 150; width: 150; height: 150
        hoverEnabled: true
        opacity: 0
        visible:false
        //onClicked: menu.state = "Preferences"
    }

}

MouseArea {
    id: mouseAreaBackImeca
    x: 480
    y: 20
    width: 20
    height: 30
    onClicked: menu.state = "Air Quality"
    visible: false
}


//------------------------------------- Estados ----------------------------------
states: [


State {
    name: "Examinar"
    PropertyChanges { target: borderImageHome4G; visible: true;}
    PropertyChanges { target: borderImageHomeUsuario; visible: true;}
    PropertyChanges { target: borderImageHomeWifi;  visible: false;}
    PropertyChanges { target: borderImageV2v;visible: true;}
    PropertyChanges { target: text5Hora; visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; visible: true;}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: borderImageHomeMultimedia; visible: true;}
    PropertyChanges { target: fileBrowser1; visible: true;}
},
State {
    name: "2"
    // Activar Visibilidad de Objetos
    PropertyChanges { target: text3Welcome; visible: true;}
    PropertyChanges { target: text4Copryht; visible: true;}
    PropertyChanges { target: mouseAreaLogo; visible: true;}
    PropertyChanges {
        target: borderImageLogo
        x: 280
        y: 87
        width: 256
        height: 256
        source: "qrc:/Imagenes/images/muves blanco color-04.png"
        opacity: 1
    }

},
State {
    name: "3"
    // Visibilidad de Objetos
    PropertyChanges { target: text5Hora; visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; visible: true;}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: text9Multimedia; visible: true;}
    PropertyChanges { target: text10Mapsx; visible: true;}
    PropertyChanges { target: text11AirQuality;  visible: true;}
    PropertyChanges { target: text12Internet;  visible: true;}
    PropertyChanges { target: text13Weather;  visible: true;}
    PropertyChanges { target: text14Settings; visible: true;}
    PropertyChanges { target: borderAirQualyty; visible: true;}
    PropertyChanges { target: borderImageHome4G; visible: true;}
    PropertyChanges { target: borderImageHomeUsuario; visible: true;}
    PropertyChanges { target: borderImageV2v;visible: true;}
    PropertyChanges { target: borderInternet;  visible: true;}
    PropertyChanges { target: borderMaps; visible: true;}
    PropertyChanges { target: borderMultimedia;  visible: true;}
    PropertyChanges { target: borderSettings; visible: true;}
    PropertyChanges { target: borderWeather; x: 334; visible: true;}
    PropertyChanges { target: mouseAreaEleccionAPP;visible: true;}
    PropertyChanges { target: mouseAreaBotonMultimedia; x: 110; y: 107; visible: true;}
    PropertyChanges { target: mouseAreaBotonMapas; x: 333; y: 107;   visible: true;}
    PropertyChanges { target: mouseAreaBotonInternet; x: 110; y: 280;  visible: true;}
    PropertyChanges { target: mouseAreaBotonAirQuality; x: 549; y: 107;  visible: true;}
    PropertyChanges { target: mouseAreaBotonWeather; x: 334; y: 280; width: 150; height: 130;  visible: true;}
    PropertyChanges { target: mouseAreaBotonSettings; x: 549; y: 280; width: 150; height: 130;  visible: true;}
    PropertyChanges { target: fileBrowser1;  visible:false}
},
State {
    name: "Mapas"
    // Activar objetos del mapa
    //cambieeeee
    PropertyChanges { target: mapaOpenStreet;  visible: true;}
    PropertyChanges { target: estacion1;  visible: false;}
    PropertyChanges { target: estacion2;  visible: false;}
    PropertyChanges { target: carro;  visible: true;}
    PropertyChanges { target: borderImageLocalizacion;  visible: true;}
    PropertyChanges { target: borderImageTrafico;  visible: true;}
    PropertyChanges { target: mouseAreaBotonLocalizacion;  visible: true;}
    PropertyChanges { target: mouseAreaBotonTeclado;  visible: true;}
    PropertyChanges { target: mouseAreaBotonTrafico;  visible: true;}
    PropertyChanges { target: mouseAreaBotonHome;  visible: true;}
    PropertyChanges { target: text5Hora; visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; visible: true;}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: borderImageHomeMultimedia;
        source: "qrc:/Imagenes/images/home mapas-01.png";
        visible: true;}
    PropertyChanges { target: mapaOpenStreet;  visible: true;}
    PropertyChanges { target: borderImageGeolocalizacion; visible: true;}
    PropertyChanges { target: borderImageHome4G; visible: true;}
    PropertyChanges { target: borderImageHomeUsuario; visible: true;}
    PropertyChanges { target: borderImageV2v; visible: true ;}
    PropertyChanges { target: fileBrowser1;  visible:false}
}
,
State {
    name: "Multimedia"
    // Activar
    PropertyChanges { target: borderImageHome4G; visible: true;}
    PropertyChanges { target: borderImageHomeUsuario; visible: true;}
    PropertyChanges { target: borderImageV2v; visible: true;}
    PropertyChanges { target: borderImageHomeMultimedia;
        source: "qrc:/Imagenes/images/home multimedia-01.png"; visible: true;}
    PropertyChanges { target: text5Hora; visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; visible: true;}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: textBuscar;  visible:true}
    PropertyChanges { target: borderImageBuscar;  visible:true}
    PropertyChanges { target: sceneSelectionPanel;  visible:false}
    PropertyChanges { target: fileBrowser1;  visible:false}
    PropertyChanges { target: mouseAreaBotonHome;  visible: true;}
    PropertyChanges { target: mouseAreaBuscarArchivos;  visible: true;}
    PropertyChanges { target: mouseAreaBuscar;  visible: true;}
},
State {
    name: "Air Quality"
    PropertyChanges { target: circle;  visible: false;}
    PropertyChanges { target: circle2;  visible: false;}
    PropertyChanges { target: accidente;  visible: false;}
    PropertyChanges { target: borderImageTecladoMapas;  visible: false;}
    PropertyChanges { target: borderImageTrafico;  visible: false;}
    PropertyChanges { target: text5Hora; visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; visible: true;}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: borderImageGeolocalizacion; visible: true;}
    PropertyChanges { target: borderImageHomeUsuario; visible: true;}
    PropertyChanges { target: borderImageHome4G; visible: true;}
    PropertyChanges { target: borderImageHomeMultimedia;
        source: "qrc:/Imagenes/images/home imeca-01.png";
        visible: true;}
    PropertyChanges { target: mapaOpenStreet;  visible: true; height:400; width:800; zoomLevel:14}
    PropertyChanges { target: estacion1;  visible: true;}
    PropertyChanges { target: estacion2;  visible: true;}
},
State {
    name: "Estacion1"
    PropertyChanges { target: circle;  visible: false;}
    PropertyChanges { target: circle2;  visible: false;}
    PropertyChanges { target: accidente;  visible: false;}
    PropertyChanges { target: borderImageTecladoMapas;  visible: false;}
    PropertyChanges { target: borderImageTrafico;  visible: false;}
    PropertyChanges { target: text5Hora; visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; visible: true;  x: 220}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: borderImageGeolocalizacion; visible: true;}
    PropertyChanges { target: borderImageHomeUsuario; visible: true;}
    PropertyChanges { target: borderImageHome4G; visible: true;}
    PropertyChanges { target: borderImageHomeMultimedia;
        source: "qrc:/Imagenes/images/home imeca-01.png";
        visible: true;}
    PropertyChanges { target: borderbackImeca; visible: true;}
    PropertyChanges { target: mouseAreaBackImeca; visible: true;}
    PropertyChanges { target: airQuality; visible: true;}
    PropertyChanges { target: mapaMiniatura; visible: true; textoT:" 1"; colorRR: "orange"; zoomLevel:10;}
    //
    PropertyChanges { target: colorO3; color: funciones.colorInttoName(VANET.getColorO3());}
    PropertyChanges { target: colorS02;  color: funciones.colorInttoName(VANET.getColorSO2());}
    PropertyChanges { target: colorN02;  color: funciones.colorInttoName(VANET.getColorNO2());}
    PropertyChanges { target: colorCO;  color: funciones.colorInttoName(VANET.getColorCO());}
    PropertyChanges { target: colorPM10;  color: funciones.colorInttoName(VANET.getColorPM10());}
    PropertyChanges { target: resumenr;  color: funciones.colorInttoName(VANET.maximoImecaString());}
    PropertyChanges { target: resumenr2;  color: funciones.colorInttoName(VANET.maximoImecaString());}
    PropertyChanges { target: nodor;  color: funciones.colorInttoName(VANET.maximoImecaString());}

},
State {
    name: "Estacion2"
    PropertyChanges { target: mouseAreaBackImeca; visible: true;}
    PropertyChanges { target: borderbackImeca; visible: true;}
    PropertyChanges { target: circle;  visible: false;}
    PropertyChanges { target: circle2;  visible: false;}
    PropertyChanges { target: accidente;  visible: false;}
    PropertyChanges { target: borderImageTecladoMapas;  visible: false;}
    PropertyChanges { target: borderImageTrafico;  visible: false;}
    PropertyChanges { target: text5Hora; visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; visible: true; x: 220}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: borderImageGeolocalizacion; visible: true;}
    PropertyChanges { target: borderImageHomeUsuario; visible: true;}
    PropertyChanges { target: borderImageHome4G; visible: true;}
    PropertyChanges { target: borderImageHomeMultimedia;
        source: "qrc:/Imagenes/images/home imeca-01.png";
        visible: true;}
    PropertyChanges { target: airQuality; visible: true;}
    PropertyChanges { target: mapaMiniatura; visible: true; textoT:" 2"; colorRR: "purple"; zoomLevel:10; textoN: "Nodo 2"}
    //
    PropertyChanges { target: colorO3; color: funciones.colorInttoName(VANET.getColorO3s2());}
    PropertyChanges { target: colorS02;  color: funciones.colorInttoName(VANET.getColorSO2s2());}
    PropertyChanges { target: colorN02;  color: funciones.colorInttoName(VANET.getColorNO2s2());}
    PropertyChanges { target: colorCO;  color: funciones.colorInttoName(VANET.getColorCOs2());}
    PropertyChanges { target: colorPM10;  color: funciones.colorInttoName(VANET.getColorPM10s2());}
    PropertyChanges { target: resumenr;  color: funciones.colorInttoName(VANET.maximoImecaStrings2());}
    PropertyChanges { target: resumenr2;  color: funciones.colorInttoName(VANET.maximoImecaStrings2());}
    PropertyChanges { target: nodor;  color: funciones.colorInttoName(VANET.maximoImecaStrings2());}
},
State {
    name: "Internet"
    PropertyChanges { target: borderImagenDia1; visible: true;
        source: "qrc:/Imagenes/images/internet.jpg"
        x: 0; y: 80; width: 800; height: 400;
    }
    PropertyChanges {
        target: borderImageHomeMultimedia
        visible: true
        source: "qrc:/Imagenes/images/home blanco-01.png"
    }
    PropertyChanges { target: borderImageHomeUsuario; visible: true;}
    PropertyChanges { target: borderImageHomeWifi;  visible: true;}
    PropertyChanges { target: text5Hora; visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; visible: true;}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: borderImageHomeMultimedia; visible: true;}

},
State {
    name: "Weather"
    PropertyChanges { target: fileBrowser1; visible: false;}
    PropertyChanges { target: borderImageHome4G; visible: true;}
    PropertyChanges { target: borderImageHomeUsuario; visible: true;}
    PropertyChanges { target: borderImageV2v;visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; visible: true;}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: borderImageHomeMultimedia; visible: true;}
    PropertyChanges { target: text5Hora; visible: true;}
    PropertyChanges { target: text6Temperatura; visible: true;}
    PropertyChanges { target: text7Fecha; x: 240; y: 38; width: 243; height: 15; visible: true;}
    PropertyChanges { target: text8Usuario; visible: true;}
    PropertyChanges { target: text5Hora; x: 70; visible: true;}
    PropertyChanges { target: wait; visible: true;}
    PropertyChanges { target: main; visible: true;}
},

State {
    name: "loading"
    PropertyChanges { target: main; opacity: 0 }
    PropertyChanges { target: wait; opacity: 1 }
},
State {
    name: "ready"
    PropertyChanges { target: main; opacity: 1 }
    PropertyChanges { target: wait; opacity: 0 }
}


]



//InputPanel {
//    id: inputPanel
//    y: Qt.inputMethod.visible ? parent.height - inputPanel.height : parent.height
//    anchors.left: parent.left
//    anchors.right: parent.right
//}


}























