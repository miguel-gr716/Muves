import QtQuick 2.0
import QtPositioning 5.5
import QtLocation 5.3
import QtMultimedia 5.0
import QtQuick.Dialogs 1.2

Text {
    id: textTemperatura
    width: 200; height: 50;
    x: 300; y: 300;
    font.pixelSize: 50
    color: "#eefaf9"
    text: qsTr("32 °C")
    styleColor: "#00ffffff"
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
    textFormat: Text.AutoText
    visible: false
}
