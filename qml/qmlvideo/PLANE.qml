// Librerias
import QtQuick 2.4
import QtLocation 5.6

MapQuickItem
{
    id: plane
    anchorPoint.x: image.width/2
    anchorPoint.y: image.height/2
    property string sourceIM
    property string colorR:"orange"
    property string texto:" 1"
    property string textoC:"White"
    property bool bandera:false

    sourceItem: Grid
    {
    columns: 1
    Grid
    {
        Rectangle
        {
            width: 25
            height: 25
            color: colorR
            visible: bandera

            Text
            {
                text: texto
                font.pixelSize: 20
                color: textoC
                styleColor: "#00ffffff"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                visible: true
            }

        }

        Image
        {
            id:image
            width: 21
            height: 27
            source: sourceIM
        }
    }
}

}


