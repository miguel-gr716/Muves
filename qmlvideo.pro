TEMPLATE = app
TARGET = target

QT += qml quick positioning positioning-private location network serialport widgets
SOURCES += main.cpp \
    vanet.cpp \
    appmodel.cpp \
    gps.cpp
RESOURCES += qmlvideo.qrc
HEADERS += \
    vanet.h \
    appmodel.h \
    gps.h \
    wifi.h

target.path = /home/osv/Descargas/Muves

INSTALLS += target

DISTFILES +=

