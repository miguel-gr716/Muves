#include "vanet.h"

VANET::VANET(QObject *parent) : QObject(parent)
{
    nam = new QNetworkAccessManager(this);
    connect(nam,SIGNAL(finished(QNetworkReply*)),this,SLOT(finished(QNetworkReply*)));
}
QString VANET::getDateTime() const
{
    return DateTime;
}

void VANET::setDateTime(const QString &value)
{
    DateTime = value;
}

QString VANET::getHour() const
{
    return Hour;
}

void VANET::setHour(const QString &value)
{
    Hour = value;
}

QString VANET::getColorPM25s2() const
{
    return colorPM25s2;
}

void VANET::setColorPM25s2(const QString &value)
{
    colorPM25s2 = value;
}

QString VANET::getColorPM10s2() const
{
    return colorPM10s2;
}

void VANET::setColorPM10s2(const QString &value)
{
    colorPM10s2 = value;
}

QString VANET::getColorCOs2() const
{
    return colorCOs2;
}

void VANET::setColorCOs2(const QString &value)
{
    colorCOs2 = value;
}

QString VANET::getColorSO2s2() const
{
    return colorSO2s2;
}

void VANET::setColorSO2s2(const QString &value)
{
    colorSO2s2 = value;
}

QString VANET::getColorNO2s2() const
{
    return colorNO2s2;
}

void VANET::setColorNO2s2(const QString &value)
{
    colorNO2s2 = value;
}

QString VANET::getColorO3s2() const
{
    return colorO3s2;
}

void VANET::setColorO3s2(const QString &value)
{
    colorO3s2 = value;
}


QString VANET::getLongitudeEstacion2() const
{
    return longitudeEstacion2;
}

void VANET::setLongitudeEstacion2(const QString &value)
{
    longitudeEstacion2 = value;
}

QString VANET::getLatitudeEstacion2() const
{
    return latitudeEstacion2;
}

void VANET::setLatitudeEstacion2(const QString &value)
{
    latitudeEstacion2 = value;
}

QString VANET::getLongitudeEstacion1() const
{
    return longitudeEstacion1;
}

void VANET::setLongitudeEstacion1(const QString &value)
{
    longitudeEstacion1 = value;
}

QString VANET::getLatitudeEstacion1() const
{
    return latitudeEstacion1;
}

void VANET::setLatitudeEstacion1(const QString &value)
{
    latitudeEstacion1 = value;
}


QString VANET::getLongitudeTrafico() const
{
    return longitudeTrafico;
}

void VANET::setLongitudeTrafico(const QString &value)
{
    longitudeTrafico = value;
}

QString VANET::getLatitudeTrafico() const
{
    return latitudeTrafico;
}

void VANET::setLatitudeTrafico(const QString &value)
{
    latitudeTrafico = value;
}

QString VANET::getLongitudeVehicle() const
{
    return longitudeVehicle;
}

void VANET::setLongitudeVehicle(const QString &value)
{
    longitudeVehicle = value;
}

QString VANET::getLatitudeVehicle() const
{
    return latitudeVehicle;
}

void VANET::setLatitudeVehicle(const QString &value)
{
    latitudeVehicle = value;
}

QString VANET::getLongitudeAccidente() const
{
    return longitudeAccidente;
}

void VANET::setLongitudeAccidente(const QString &value)
{
    longitudeAccidente = value;
}

QString VANET::getLatitudeAccidente() const
{
    return latitudeAccidente;
}

void VANET::setLatitudeAccidente(const QString &value)
{
    latitudeAccidente = value;
}

QString VANET::getPM25() const
{
    return mPM25;
}

void VANET::setPM25(const QString &pM25)
{
    mPM25 = pM25;
}

QString VANET::getPM10() const
{
    return mPM10;
}

void VANET::setPM10(const QString &pM10)
{
    mPM10 = pM10;
}

QString VANET::getNO2() const
{
    return mNO2;
}

void VANET::setNO2(const QString &nO2)
{
    mNO2 = nO2;
}

QString VANET::getO3() const
{
    return mO3;
}

void VANET::setO3(const QString &o3)
{
    mO3 = o3;
}

QString VANET::getColorPM25() const
{
    return colorPM25;
}

void VANET::setColorPM25(const QString &value)
{
    colorPM25 = value;
}

QString VANET::getImecaPM25() const
{
    return imecaPM25;
}

void VANET::setImecaPM25(const QString &value)
{
    imecaPM25 = value;
}

QString VANET::getColorPM10() const
{
    return colorPM10;
}

void VANET::setColorPM10(const QString &value)
{
    colorPM10 = value;
}

QString VANET::getImecaPM10() const
{
    return imecaPM10;
}

void VANET::setImecaPM10(const QString &value)
{
    imecaPM10 = value;
}

QString VANET::getColorCO() const
{
    return colorCO;
}

void VANET::setColorCO(const QString &value)
{
    colorCO = value;
}

QString VANET::getImecaCO() const
{
    return imecaCO;
}

void VANET::setImecaCO(const QString &value)
{
    imecaCO = value;
}

QString VANET::getColorSO2() const
{
    return colorSO2;
}

void VANET::setColorSO2(const QString &value)
{
    colorSO2 = value;
}

QString VANET::getImecaSO2() const
{
    return imecaSO2;
}

void VANET::setImecaSO2(const QString &value)
{
    imecaSO2 = value;
}

QString VANET::getColorNO2() const
{
    return colorNO2;
}

void VANET::setColorNO2(const QString &value)
{
    colorNO2 = value;
}

QString VANET::getImecaNO2() const
{
    return imecaNO2;
}

void VANET::setImecaNO2(const QString &value)
{
    imecaNO2 = value;
}

QString VANET::getColorO3() const
{
    return colorO3;
}

void VANET::setColorO3(const QString &value)
{
    colorO3 = value;
}

QString VANET::getImecaO3() const
{
    return imecaO3;
}

void VANET::setImecaO3(const QString &value)
{
    imecaO3 = value;
}

QString VANET::getJamAvgSpeed() const
{
    return jamAvgSpeed;
}

void VANET::setJamAvgSpeed(const QString &value)
{
    jamAvgSpeed = value;
}

QString VANET::getJamLength() const
{
    return jamLength;
}

void VANET::setJamLength(const QString &value)
{
    jamLength = value;
}

QString VANET::getJamAlertId() const
{
    return jamAlertId;
}

void VANET::setJamAlertId(const QString &value)
{
    jamAlertId = value;
}

QString VANET::getEnvSearchRadius() const
{
    return envSearchRadius;
}

void VANET::setEnvSearchRadius(const QString &value)
{
    envSearchRadius = value;
}

QString VANET::getStation_ip() const
{
    return station_ip;
}

void VANET::setStation_ip(const QString &value)
{
    station_ip = value;
}

QString VANET::getStation_id() const
{
    return station_id;
}

void VANET::setStation_id(const QString &value)
{
    station_id = value;
}

QString VANET::getVehicle_ip() const
{
    return vehicle_ip;
}

void VANET::setVehicle_ip(const QString &value)
{
    vehicle_ip = value;
}

QString VANET::getVehicle_id() const
{
    return vehicle_id;
}

void VANET::setVehicle_id(const QString &value)
{
    vehicle_id = value;
}

QString VANET::getJamDetectionTimeFrame() const
{
    return jamDetectionTimeFrame;
}

void VANET::setJamDetectionTimeFrame(const QString &value)
{
    jamDetectionTimeFrame = value;
}

QString VANET::getJamSearchRadius() const
{
    return jamSearchRadius;
}

void VANET::setJamSearchRadius(const QString &value)
{
    jamSearchRadius = value;
}

QString VANET::getTimestamp() const
{
    return timestamp;
}

void VANET::setTimestamp(const QString &value)
{
    timestamp = value;
}

QString VANET::getSpeed() const
{
    return speed;
}

void VANET::setSpeed(const QString &value)
{
    speed = value;
}

QString VANET::getPackageCounter() const
{
    return packageCounter;
}

void VANET::setPackageCounter(const QString &value)
{
    packageCounter = value;
}

QString VANET::getTemperature() const
{
    return temperature;
}

void VANET::setTemperature(const QString &value)
{
    temperature = value;
}

QString VANET::getLongitude() const
{
    return longitude;
}

void VANET::setLongitude(const QString &value)
{
    longitude = value;
}

QString VANET::getLatitude() const
{
    return latitude;
}

void VANET::setLatitude(const QString &value)
{
    latitude = value;
}

void VANET::setAcknowledgement(const QString &value)
{
    acknowledgement = value;
}


QString VANET::getRespuesta() const
{
    return respuesta;
}


void VANET::insertVehicle(QString acknowledgement,QString latitude,QString temperature,QString vehicleId,QString packageCounter,QString speed,QString timestamp,QString longitude,QString vehicleIp)
{
    QString url = urlVehicleInsert;
    QString paras="{\"acknowledgement\":\""+acknowledgement+"\",\"latitude\":\""+latitude+"\",\"temperature\":\""+temperature+"\",\"vehicleId\":\""+vehicleId+"\",\"packageCounter\":\""+packageCounter+"\",\"speed\":\""+speed+"\",\"timestamp\":\""+timestamp+"\",\"longitude\":\""+longitude+"\", \"vehicleIp\":\" " +vehicleIp+ "\"}";
    QByteArray post_data;
    post_data.append(paras);
    QNetworkRequest request= QNetworkRequest(QUrl(url));
    request.setRawHeader("Content-Type", "application/json");
    nam->post(request,post_data);
}

void VANET::insertVehicle_(QString acknowledgement, QString latitude, QString temperature, QString vehicleId, QString packageCounter, QString speed, QString timestamp, QString longitude, QString vehicleIp)
{
    QString url = urlVehicleInsert_;
    QString paras="{\"acknowledgement\":\""+acknowledgement+"\",\"latitude\":\""+latitude+"\",\"temperature\":\""+temperature+"\",\"vehicleId\":\""+vehicleId+"\",\"packageCounter\":\""+packageCounter+"\",\"speed\":\""+speed+"\",\"timestamp\":\""+timestamp+"\",\"longitude\":\""+longitude+"\",\"vehicleIp\":\"" +vehicleIp+"\"}";
    QByteArray post_data;
    post_data.append(paras);
    QNetworkRequest request= QNetworkRequest(QUrl(url));
    request.setRawHeader("Content-Type", "application/json");
    nam->post(request,post_data);
}

void VANET::ultimaLecturaTrafico(QString vehicleId, QString timestamp, QString longitude, QString latitude, QString jamSearchRadius, QString jamDetectionTimeFrame, QString vehicleIp)
{
    QString url = urlUltimaLecturaTrafico;
    QString paras="{\"vehicleId\":\""+vehicleId+"\",\"timestamp\":\""+timestamp+"\",\"longitude\":\""+longitude+"\",\"latitude\":\""+latitude+"\",\"jamSearchRadius\":\""+jamSearchRadius+"\",\"jamDetectionTimeFrame\":\""+jamDetectionTimeFrame+"\",\"vehicleIp\":\"" + vehicleIp + "\"}";
    QByteArray post_data;
    post_data.append(paras);
    QNetworkRequest request= QNetworkRequest(QUrl(url));
    request.setRawHeader("Content-Type", "application/json");
    nam->post(request,post_data);
}

void VANET::ultimaLecturaIndicadores(QString vehicleId, QString timestamp, QString longitude, QString latitude, QString envSearchRadius, QString vehicleIp)
{
    QString url = urlUltimaLecIndAmbientales;
    QString paras="{\"vehicleId\":\""+vehicleId+"\",\"timestamp\":\""+timestamp+"\",\"longitude\":\""+longitude+"\",\"latitude\":\""+latitude+"\",\"envSearchRadius\":\""+envSearchRadius+"\",\"vehicleIp\":\""+vehicleIp+"\"}";
    QByteArray post_data;
    post_data.append(paras);
    QNetworkRequest request= QNetworkRequest(QUrl(url));
    request.setRawHeader("Content-Type", "application/json");
    nam->post(request,post_data);
}

void VANET::insertIndicadoresAmbientales(QString stationId, QString stationIp, QString timestamp, QString mO3, QString mNO2, QString mSO2, QString mCO, QString mPM10, QString mPM25, QString packageCounter, QString acknowledgement)
{
    QString url = urlIndicadoresAmbientalesInsert;
    QString paras="{\"stationId\":\""+stationId+"\",\"stationIp\":\""+ stationIp +"\",\"timestamp\":\"" + timestamp + "\",\"mO3\":\"" +mO3+ "\",\"mNO2\":\"" + mNO2 +"\",\"mSO2\":\""+ mSO2 +"\",\"mCO\":\"" + mCO + "\", \"mPM10\":\"" + mPM10 + "\",\"mPM25\":\"" + mPM25 + "\",\"packageCounter\":\"" + packageCounter + "\",\"acknowledgement\":\""+acknowledgement+"\"}";
    QByteArray post_data;
    post_data.append(paras);
    QNetworkRequest request= QNetworkRequest(QUrl(url));
    request.setRawHeader("Content-Type", "application/json");
    nam->post(request,post_data);
}

void VANET::insertIndicadoresAmbientales_(QString stationId, QString stationIp, QString timestamp, QString mO3, QString mNO2, QString mSO2, QString mCO, QString mPM10, QString mPM25, QString packageCounter, QString acknowledgement)
{
    QString url = urlIndicadoresAmbientalesInsert_;
    QString paras="{\"stationId\":\""+stationId+"\",\"stationIp\":\""+ stationIp +"\",\"timestamp\":\"" + timestamp + "\",\"mO3\":\"" +mO3+ "\",\"mNO2\":\"" + mNO2 +"\",\"mSO2\":\""+ mSO2 +"\",\"mCO\":\"" + mCO + "\", \"mPM10\":\"" + mPM10 + "\",\"mPM25\":\"" + mPM25 + "\",\"packageCounter\":\"" + packageCounter + "\",\"acknowledgement\":\""+acknowledgement+"\"}";

    QByteArray post_data;
    post_data.append(paras);
    QNetworkRequest request= QNetworkRequest(QUrl(url));
    request.setRawHeader("Content-Type", "application/json");
    nam->post(request,post_data);
}

int VANET::maximoImeca()
{
    int a,b,c,d,e,maximo;

    a = colorCO.toInt();
    b = colorNO2.toInt();
    c = colorO3.toInt();
    d = colorPM10.toInt();
    e = colorSO2.toInt();

    if(a>=b && a>=c && a>=d && a>=e) maximo = a;
    if(b>=a && b>=c && b>=d && b>=e) maximo = b;
    if(c>=a && c>=b && c>=d && c>=e) maximo = c;
    if(d>=a && d>=b && d>=c && d>=e) maximo = d;
    if(e>=a && e>=b && e>=c && e>=d) maximo = e;

    return maximo;
}

QString VANET::maximoImecaString()
{
    return QString::number(maximoImeca());
}

int VANET::maximoImecas2()
{
    int a,b,c,d,e,maximo;

    a = colorCOs2.toInt();
    b = colorNO2s2.toInt();
    c = colorO3s2.toInt();
    d = colorPM10s2.toInt();
    e = colorSO2s2.toInt();

    if(a>=b && a>=c && a>=d && a>=e) maximo = a;
    if(b>=a && b>=c && b>=d && b>=e) maximo = b;
    if(c>=a && c>=b && c>=d && c>=e) maximo = c;
    if(d>=a && d>=b && d>=c && d>=e) maximo = d;
    if(e>=a && e>=b && e>=c && e>=d) maximo = e;

    return maximo;
}

QString VANET::maximoImecaStrings2()
{
   return QString::number(maximoImecas2());
}

void VANET::finished(QNetworkReply *reply)
{
    if(reply->error() == QNetworkReply::NoError)
    {
        //*******insert
        //QByteArray respuestaArray = pruebaInsertVehicle1;
        //QByteArray respuestaArray = pruebaInsertVehicle2;
        //QByteArray respuestaArray = pruebaInsertIndAmb1;
        //QByteArray respuestaArray = pruebaInsertIndAmb2;
        //QByteArray respuestaArray = pruebaPeticionTrafico1;
        //QByteArray respuestaArray = pruebaPeticionTrafico2;
        //QByteArray respuestaArray = pruebaPeticionCalidadAire1;
        //QByteArray respuestaArray = pruebaPeticionCalidadAire2;
        QByteArray respuestaArray = reply->readAll();
        qDebug() << respuestaArray;
        QJsonDocument document = QJsonDocument::fromJson(respuestaArray);
        QJsonObject root = document.object();
        QJsonValue jv = root.value("list");

        //        for (int var = 0; var < root.count(); ++var) {
        //            qDebug() << root.keys().at(var);
        //        }

        if(jv.isArray())
        {
            int contador = 0;
            QJsonArray ja = jv.toArray();
            for (int i = 0; i < ja.count(); ++i)
            {
                QJsonObject subtree = ja.at(i).toObject();

                if(subtree.value("vehicleId").toString() != "") contador++;
                if(subtree.value("vehicleIp").toString() != "") contador++;
                if(subtree.value("stationIp").toString() != "") contador++;
                if(subtree.value("stationId").toString() != "") contador++;
                if(subtree.value("jamAlertId").toString() != "") contador++;
                if(subtree.value("timestamp").toString() != "") contador++;
                if(subtree.value("longitude").toString() != "") contador++;
                if(subtree.value("latitude").toString() != "") contador++;
                if(subtree.value("jamLength").toString() != "") contador++;
                if(subtree.value("jamAvgSpeed").toString() != "") contador++;
                if(subtree.value("imecaO3").toString() != "") contador++;
                if(subtree.value("colorO3").toString() != "") contador++;
                if(subtree.value("imecaNO2").toString() != "") contador++;
                if(subtree.value("colorNO2").toString() != "") contador++;
                if(subtree.value("imecaSO2").toString() != "") contador++;
                if(subtree.value("colorSO2").toString() != "") contador++;
                if(subtree.value("imecaCO").toString() != "") contador++;
                if(subtree.value("colorCO").toString() != "") contador++;
                if(subtree.value("imecaPM10").toString() != "") contador++;
                if(subtree.value("imecaPM25").toString() != "") contador++;
                if(subtree.value("colorPM10").toString() != "") contador++;
                if(subtree.value("colorPM25").toString() != "") contador++;

                qDebug() << contador;


                QString a = subtree.value("vehicleId").toString();
                QString b = subtree.value("vehicleIp").toString();
                QString d = subtree.value("stationId").toString();
                QString c = subtree.value("stationIp").toString();

                switch (contador) {
                case 2:
                    if(a != "")
                    {
                        vehicle_id = a;
                        qDebug() << "vehicleId:  "+ vehicle_id + " ";
                    }
                    if(b != "")
                    {
                        vehicle_ip = b;
                        qDebug() << "vehicleIp:  "+ vehicle_ip + " ";
                    }
                    if(c != "")
                    {
                        station_id = c;
                        qDebug() << "stationId:  "+ station_id + " ";
                    }
                    if(d != "")
                    {
                        station_ip = d;
                        qDebug() << "stationIp:  "+ station_ip + " ";
                    }

                    if(a != "" && b != "")
                    {
                        respuesta = "insertVehicle";
                        qDebug() << "respuesta:  " + respuesta;
                    }
                    if(c != "" && d != "")
                    {
                        respuesta = "insertIndAmb";
                        qDebug() << "respuesta:  "+ respuesta;
                    }

                    break;
                case 8:
                    vehicle_id = subtree.value("vehicleId").toString();
                    qDebug() << "vehicleId:  "+ vehicle_id + " ";
                    vehicle_ip = subtree.value("vehicleIp").toString();
                    qDebug() << "vehicleIp:  "+ vehicle_ip + " ";
                    jamAlertId = subtree.value("jamAlertId").toString();
                    qDebug() << "jamAlertId:  "+ jamAlertId + " ";
                    jamLength = subtree.value("jamLength").toString();
                    qDebug() << "jamLength:  "+ jamLength + " ";
                    jamAvgSpeed = subtree.value("jamAvgSpeed").toString();
                    qDebug() << "jamAvgSpeed:  "+ jamAvgSpeed + " ";
                    longitude = subtree.value("longitude").toString();
                    qDebug() << "longitude:  "+ longitude + " ";
                    latitude = subtree.value("latitude").toString();
                    qDebug() << "latitude:  "+ latitude + " ";
                    timestamp = subtree.value("timestamp").toString();
                    qDebug() << "timestamp:  "+ timestamp + " ";
                    respuesta = "lecTrafico";
                    qDebug() << "respuesta:  "+ respuesta;
                    break;
                case 16:
                    station_id = subtree.value("stationId").toString();
                    qDebug() << "stationId:  "+ station_id + " ";
                    vehicle_ip = subtree.value("vehicleIp").toString();
                    qDebug() << "vehicleIp:  "+ vehicle_ip + " ";
                    vehicle_id = subtree.value("vehicleId").toString();
                    qDebug() << "vehicleId:  "+ vehicle_id + " ";
                    timestamp = subtree.value("timestamp").toString();
                    qDebug() << "timestamp:  "+ timestamp + " ";
                    imecaO3 = subtree.value("imecaO3").toString();
                    qDebug() << "imecaO3:  "+ imecaO3 + " ";
                    colorO3 = subtree.value("colorO3").toString();
                    qDebug() << "colorO3:  "+ colorO3 + " ";
                    imecaNO2 = subtree.value("imecaNO2").toString();
                    qDebug() << "imecaNO2:  "+ imecaNO2 + " ";
                    colorNO2 = subtree.value("colorNO2").toString();
                    qDebug() << "colorNO2:  "+ colorNO2 + " ";
                    imecaSO2 = subtree.value("imecaSO2").toString();
                    qDebug() << "imecaSO2:  "+ imecaSO2 + " ";
                    colorSO2 = subtree.value("colorSO2").toString();
                    qDebug() << "colorSO2:  "+ colorSO2 + " ";
                    colorCO = subtree.value("colorCO").toString();
                    qDebug() << "colorCO:  "+ colorCO + " ";
                    colorCO = subtree.value("colorCO").toString();
                    qDebug() << "colorCO:  "+ colorCO + " ";
                    imecaPM10 = subtree.value("imecaPM10").toString();
                    qDebug() << "imecaPM10:  "+ imecaPM10 + " ";
                    colorPM10 = subtree.value("colorPM10").toString();
                    qDebug() << "colorPM10:  "+ colorPM10 + " ";
                    imecaPM25 = subtree.value("imecaPM25").toString();
                    qDebug() << "imecaPM25:  "+ imecaPM25 + " ";
                    colorPM25 = subtree.value("colorPM25").toString();
                    qDebug() << "colorPM25:  "+ colorPM25 + " ";
                    respuesta = "lecIndAmbientales";
                    qDebug() << "respuesta:  "+ respuesta;
                    break;
                }

                contador = 0;
            }

        }
    }
}





