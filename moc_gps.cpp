/****************************************************************************
** Meta object code from reading C++ file 'gps.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "gps.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gps.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_GPS_t {
    QByteArrayData data[25];
    char stringdata0[283];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GPS_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GPS_t qt_meta_stringdata_GPS = {
    {
QT_MOC_LITERAL(0, 0, 3), // "GPS"
QT_MOC_LITERAL(1, 4, 14), // "openSerialPort"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 15), // "closeSerialPort"
QT_MOC_LITERAL(4, 36, 9), // "writeData"
QT_MOC_LITERAL(5, 46, 4), // "data"
QT_MOC_LITERAL(6, 51, 8), // "readData"
QT_MOC_LITERAL(7, 60, 11), // "handleError"
QT_MOC_LITERAL(8, 72, 28), // "QSerialPort::SerialPortError"
QT_MOC_LITERAL(9, 101, 5), // "error"
QT_MOC_LITERAL(10, 107, 22), // "interpretarSuperCadena"
QT_MOC_LITERAL(11, 130, 6), // "cadena"
QT_MOC_LITERAL(12, 137, 16), // "procesarLongitud"
QT_MOC_LITERAL(13, 154, 1), // "l"
QT_MOC_LITERAL(14, 156, 15), // "procesarLatitud"
QT_MOC_LITERAL(15, 172, 8), // "latitudL"
QT_MOC_LITERAL(16, 181, 10), // "getLatitud"
QT_MOC_LITERAL(17, 192, 10), // "setLatitud"
QT_MOC_LITERAL(18, 203, 5), // "value"
QT_MOC_LITERAL(19, 209, 11), // "getLongitud"
QT_MOC_LITERAL(20, 221, 11), // "setLongitud"
QT_MOC_LITERAL(21, 233, 12), // "getVelocidad"
QT_MOC_LITERAL(22, 246, 12), // "setVelocidad"
QT_MOC_LITERAL(23, 259, 11), // "getPosicion"
QT_MOC_LITERAL(24, 271, 11) // "setPosicion"

    },
    "GPS\0openSerialPort\0\0closeSerialPort\0"
    "writeData\0data\0readData\0handleError\0"
    "QSerialPort::SerialPortError\0error\0"
    "interpretarSuperCadena\0cadena\0"
    "procesarLongitud\0l\0procesarLatitud\0"
    "latitudL\0getLatitud\0setLatitud\0value\0"
    "getLongitud\0setLongitud\0getVelocidad\0"
    "setVelocidad\0getPosicion\0setPosicion"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GPS[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x0a /* Public */,
       3,    0,   95,    2, 0x0a /* Public */,
       4,    1,   96,    2, 0x0a /* Public */,
       6,    0,   99,    2, 0x0a /* Public */,
       7,    1,  100,    2, 0x0a /* Public */,
      10,    1,  103,    2, 0x0a /* Public */,
      12,    1,  106,    2, 0x0a /* Public */,
      14,    1,  109,    2, 0x0a /* Public */,
      16,    0,  112,    2, 0x0a /* Public */,
      17,    1,  113,    2, 0x0a /* Public */,
      19,    0,  116,    2, 0x0a /* Public */,
      20,    1,  117,    2, 0x0a /* Public */,
      21,    0,  120,    2, 0x0a /* Public */,
      22,    1,  121,    2, 0x0a /* Public */,
      23,    0,  124,    2, 0x0a /* Public */,
      24,    1,  125,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::QString, QMetaType::QString,   11,
    QMetaType::QString, QMetaType::QString,   13,
    QMetaType::QString, QMetaType::QString,   15,
    QMetaType::Double,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Double,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   18,

       0        // eod
};

void GPS::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        GPS *_t = static_cast<GPS *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->openSerialPort(); break;
        case 1: _t->closeSerialPort(); break;
        case 2: _t->writeData((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->readData(); break;
        case 4: _t->handleError((*reinterpret_cast< QSerialPort::SerialPortError(*)>(_a[1]))); break;
        case 5: { QString _r = _t->interpretarSuperCadena((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 6: { QString _r = _t->procesarLongitud((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 7: { QString _r = _t->procesarLatitud((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 8: { double _r = _t->getLatitud();
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = _r; }  break;
        case 9: _t->setLatitud((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: { double _r = _t->getLongitud();
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = _r; }  break;
        case 11: _t->setLongitud((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: { QString _r = _t->getVelocidad();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 13: _t->setVelocidad((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: { QString _r = _t->getPosicion();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 15: _t->setPosicion((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject GPS::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_GPS.data,
      qt_meta_data_GPS,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *GPS::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GPS::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_GPS.stringdata0))
        return static_cast<void*>(const_cast< GPS*>(this));
    return QObject::qt_metacast(_clname);
}

int GPS::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
